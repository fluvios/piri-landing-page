<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); 

$moto_opt = moto_get_opt();

$single_layout = 'fullw';
if(isset($moto_opt['moto_shop_single_page_layout']) && $moto_opt['moto_shop_single_page_layout']!=''){
	$single_layout = $moto_opt['moto_shop_single_page_layout'];
}
$singleclass = 12;
switch($single_layout){
	case 'fullw':
	$singleclass = 12;
	break;
	default:
	$singleclass = 9;
} 

?>

<div class="page-area ptb-80 single-product-area">
	<div class="container">
		<div class="row">
			<?php
				/**
				 * woocommerce_before_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				remove_action('woocommerce_before_main_content','woocommerce_breadcrumb',20);
				do_action( 'woocommerce_before_main_content' );
			?>
			
			<?php 
				if($single_layout=='left'):
				/**
				 * woocommerce_sidebar hook.
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				do_action( 'woocommerce_sidebar' );
				endif;
			?>
			
			<div class="col-xs-12 <?php echo 'col-md-'.$singleclass; ?>">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'single-product' ); ?>

				<?php endwhile; // end of the loop. ?>
				
			</div>	
			<?php 
				if($single_layout=='right'):
				/**
				 * woocommerce_sidebar hook.
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				do_action( 'woocommerce_sidebar' );
				endif;
			?>

			<?php
				/**
				 * woocommerce_after_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action( 'woocommerce_after_main_content' );
			?>
		</div>
	</div>
</div>

<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
