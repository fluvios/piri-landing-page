<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */
    if ( ! class_exists( 'Redux' ) ) {
        return;
    }
    // This is your option name where all the Redux data is stored.
    $opt_name = "moto_opt";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );
    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'submenu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Theme Options', 'moto' ),
        'page_title'           => esc_html__( 'Theme Options', 'moto' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true, 
        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                 => array(
            'icon'              => 'el el-question-sign',
            'icon_position'     => 'right',
            'icon_color'        => 'lightgray',
            'icon_size'         => 'normal',
            'tip_style'         => array(
                'color'     => 'red',
                'shadow'    => true,
                'rounded'   => false,
                'style'     => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => esc_html__( 'Documentation', 'moto' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => esc_html__( 'Support', 'moto' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => esc_html__( 'Extensions', 'moto' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/devitems',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://twitter.com/devitemsllc',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.linkedin.com/in/devitems-llc-a87b38106/',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.behance.net/DevItems',
        'title' => 'Find us on Behance',
        'icon'  => 'el el-behance'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://dribbble.com/devitems',
        'title' => 'Find us on Dribbble',
        'icon'  => 'el el-dribbble'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.instagram.com/devitems/',
        'title' => 'Find us on Instagram',
        'icon'  => 'el el-instagram'
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */
    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'moto' ),
            'content' => esc_html__( 'This is the tab content, HTML is allowed.', 'moto' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'moto' ),
            'content' => esc_html__( 'This is the tab content, HTML is allowed.', 'moto' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( 'This is the sidebar content, HTML is allowed.', 'moto' );
    Redux::setHelpSidebar( $opt_name, $content );
    /*
     * <--- END HELP TABS
     */
    // -> START Basic Fields
    //layout
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Layout', 'moto' ),
        'id'               => 'moto_layout',
        'customizer_width' => '400px',
        'icon'             => 'el el-website',
        'fields'           => array(
                array(
                    'id'                    => 'moto_layout_width',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__('Theme Layout', 'moto'),
                    'subtitle'              => esc_html__('Controls the site layout.', 'moto'),
                    'options'               => array(
                        'wide-layout'       => esc_html__('Full Width',  'moto'),
                        'boxed-layout'      => esc_html__('Boxed', 'moto'), 
                     ), 
                    'default'               => 'wide-layout'
                ),
                array(
                    'id'                    => 'moto_layout_page',
                    'type'                  => 'text',
                    'required'              => array('moto_layout_width','equals','wide-layout'),
                    'title'                 => esc_html__('Site Width', 'moto'),
                    'subtitle'              => esc_html__('Controls the overall site width. Enter value excluding any valid CSS unit, ex: 1170', 'moto'),
                ),
                array(
                    'id'                    => 'moto_boxlayout_box_width',
                    'type'                  => 'text',
                    'required'              => array('moto_layout_width','equals','boxed-layout'),
                    'title'                 => esc_html__('Site Width For Box Layout', 'moto'),
                    'subtitle'              => esc_html__('Controls the overall site width. Enter value excluding any valid CSS unit, ex: 1170', 'moto'),
                ),
                array(
                    'id'                    => 'moto_boxlayout_body_bg',
                    'type'                  => 'background',
                    'required'              => array( 'moto_layout_width','equals','boxed-layout'),
                    'output'                => array('body.boxed-layout-active'),
                    'title'                 => esc_html__('Box Layout Background', 'moto'),
                    'subtitle'              => esc_html__('Controls the background color when the site is in box layout.', 'moto'),
                    'default'               => array(
                        
                    )
                ),
                array(
                    'id'                    => 'moto_page_layout_padding',
                    'type'                  => 'spacing',
                    'output'                => array('.page-area'),
                    'title'                 => esc_html__('Page Content Padding', 'moto'),
                    'subtitle'              => esc_html__('Controls the top /bottom padding for page content. Enter values including any valid CSS unit, ex: 80px,80px.', 'moto'),
                    'mode'                  => 'padding',
                    'units'                 => array('em','px'),
                    'units_extended'        => 'false',
                    'left'                  => 'false',
                    'right'                 => 'false'
                ),
                array(
                    'id'                    => 'moto_body_background_color',
                    'type'                  => 'background',
                    'output'                => array('body, .site-wrapper.boxed-layout'),
                    'title'                 => esc_html__('Body Background Color.', 'moto'),
                    'subtitle'              => esc_html__('Controls the  background color of the body which is everything below header and above footer.', 'moto'),
                ),

            )
        ) 
    );

  //Header top bar
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header', 'moto' ),
        'id'         => 'header_id',
        'icon'       =>'el el-arrow-up',
        'fields'     => array(
                array(
                    'id'                    => 'moto_header_bg',
                    'type'                  => 'background',
                    'background-attachment' => false,
                    'background-repeat'     => false,
                    'background-size'       => false,
                    'background-position'   => false,
                    'background-image'      => false,
                    'output'                => array('.header-area'),
                    'title'                 => esc_html__('Header Background color', 'moto'),
                    'subtitle'              => esc_html__('Controls the header background color( default #ffffff ).', 'moto'),
                    'default'               => array(
                        
                    )
                ),
                array(
                    'id'                    => 'moto_header_layout',
                    'title'                 => esc_html__('Select Header Layout', 'moto'),
                    'subtitle'              => esc_html__('Controls the header layout.', 'moto'),
                    'type'                  => 'image_select',
                    'options'               => array(
                        '1'                 => array(
                            'title'         => esc_html__('Header Layout One', 'moto'),
                            'img'           => get_template_directory_uri().'/images/optionframework/style1.png',
                        ),
                        '2'                 => array(
                            'title'         => esc_html__('Header Layout Two', 'moto'),
                            'img'           => get_template_directory_uri(). '/images/optionframework/style2.png',
                        ),
                        '3'                 => array(
                            'title'         => esc_html__('Header One Page', 'moto'),
                            'img'           => get_template_directory_uri(). '/images/optionframework/style1.png',
                        ),
                    ),
                    'default'               => '1'
                ),
                array(
                    'id'                    => 'moto_logo_position',
                    'type'                  => 'button_set',
                    'required'              => array('moto_header_layout','equals',array('1','3')),
                    'title'                 => esc_html__('Logo Position','moto'),
                    'subtitle'              => esc_html__('Controls the position of the logo.','moto'),
                    'options'               => array(
                        'left'              => esc_html__('Left','moto'),
                        'center'            => esc_html__('Center','moto'),
                        'right'             => esc_html__('Right','moto'),
                        'left-top'          => esc_html__('Left Top','moto'),
                        'center-top'        => esc_html__('Center Top','moto'),
                        'right-top'         => esc_html__('Right Top','moto'),
                    ), 
                    'default'               => 'left'
                ),
                array(
                    'id'                    => 'moto_header_full_width',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Full Width Header', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the width of the header area. ', 'moto' ),
                    'default'               => false,
                ),
                array(
                    'id'                    => 'moto_header_sticky',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Sticky Header', 'moto' ),
                    'subtitle'              => esc_html__( 'Turn on to activate the sticky header.', 'moto' ),
                    'default'               => true,
                ),
                array(
                    'id'                    => 'moto_header_transparent',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Transparent Header', 'moto' ),
                    'subtitle'              => esc_html__( 'Turn on to make the header area transparent.', 'moto' ),
                    'default'               => false,
                ),

            )
        ) 
    );
  //Header top bar left
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Top Bar left', 'moto' ),
        'id'         => 'moto_header_left',
        'icon'       =>'el el-arrow-right',
        'subsection' => true,
        'fields'     => array(
                array(
                    'id'                    => 'moto_left_content_section',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__( 'Top Bar Left Content', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the content that displays in the top left section.', 'moto' ),
                    'options'               => array(
                        '1'                 => esc_html__('Social Icon','moto'),
                        '2'                 => esc_html__('Left Menu','moto'),      
                        '3'                 => esc_html__('Contact Info','moto'),
                        '4'                 => esc_html__('Content','moto'),
                        '5'                 => esc_html__('Leave Empty','moto'),
                    ),
                    'default'               => '3'
                ),
                array(
                    'id'                    => 'moto_left_contact_info',
                    'type'                  => 'text',
                    'required'              => array('moto_left_content_section','equals','3'),
                    'title'                 => esc_html__( 'Header Contact Number', 'moto' ),
                    'subtitle'              => esc_html__( 'This content will display header contact number on top left bar', 'moto' ),
                    'default'               => 'Call Us +0123456789',
                ),
                array(
                    'id'                    => 'moto_left_contact_email',
                    'type'                  => 'text',
                    'required'              => array('moto_left_content_section','equals','3'),
                    'title'                 => esc_html__( 'Header Contact Email Address', 'moto' ),
                    'subtitle'              => esc_html__( 'This content will display header contact email address on top left bar', 'moto' ),
                    'default'               => 'info@hashdemo.com',
                ),
                array(
                    'id'                    => 'moto_left_text_area',
                    'type'                  => 'editor',
                    'required'              => array('moto_left_content_section','equals','4'),
                    'title'                 => esc_html__( 'Header Default Text', 'moto' ),
                    'subtitle'              => esc_html__( 'This content will display the default text on top left bar', 'moto' ),
                    'args'                  => array(
                        'teeny'             => true,
                        'textarea_rows'     => 2
                    ),
                ),
            )
        ) 
    );
  //Header top bar right
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Top Right Bar', 'moto' ),
        'id'         => 'moto_header_right',
        'icon'       =>'el el-arrow-right',
        'subsection'=> true,
        'fields'     => array(
                array(
                    'id'                    => 'moto_right_contactinfo',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__( 'Top Bar Right Content', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the content that displays in the top left section.', 'moto' ),
                    'options'               => array(
                        '1'                 => esc_html__('Social Icon','moto'),                        
                        '2'                 => esc_html__('Right Menu','moto'),                        
                        '3'                 => esc_html__('Contact Info','moto'),                        
                        '4'                 => esc_html__('Content','moto'),                        
                        '5'                 => esc_html__('Leave Empty','moto'),                        
                    ),
                    'default'               => '3'
                ),
                array(
                    'id'                    => 'moto_right_contact_info',
                    'type'                  => 'text',
                    'required'              => array('moto_right_contactinfo','equals','3'),
                    'title'                 => esc_html__( 'Header Contact Number', 'moto' ),
                    'subtitle'              => esc_html__( 'This content will display header contact number on top right bar.', 'moto' ),
                    'default'               => 'Call Us +0123456789',
                ),
                array(
                    'id'                    => 'moto_right_contact_email',
                    'type'                  => 'text',
                    'required'              => array('moto_right_contactinfo','equals','3'),
                    'title'                 => esc_html__( 'Email Address For Contact Info', 'moto' ),
                    'subtitle'              => esc_html__( 'This content will display if you have "Contact Info" email.', 'moto' ),
                    'default'               => 'info@hashdemo.com',
                ),
                array(
                    'id'                    => 'moto_right_text_area',
                    'type'                  => 'editor',
                    'required'              => array('moto_right_contactinfo','equals','4'),
                    'title'                 => esc_html__( 'Header Default Text', 'moto' ),
                    'subtitle'              => esc_html__( 'This content will display the default text on top right bar', 'moto' ),
                    'args'                  => array(
                        'teeny'             => true,
                        'textarea_rows'     => 2
                    )
                ),
            )
        ) 
    );
  //Header top bar right
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Topbar Customize', 'moto' ),
        'id'         => 'moto_header_topbar_customize',
        'icon'       =>'el el-arrow-right',
        'subsection'=> true,
        'fields'     => array(
                array(
                    'id'                    => 'moto_header_show',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Top Bar', 'moto' ),
                    'subtitle'              => esc_html__( 'Turn on if you want to show the top bar area.', 'moto' ),
                    'default'               => false,
                ),
                array(
                    'id'                    => 'moto_header_top_width',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Full Width Top Bar', 'moto' ),
                    'subtitle'              => esc_html__( 'Turn on if you want the top bar to be of full width.', 'moto' ),
                    'default'               => false,
                ),
                array(
                    'id'                    => 'moto_header_topbg',
                    'type'                  => 'background',
                    'background-attachment' => false,
                    'background-repeat'     => false,
                    'background-size'       => false,
                    'background-position'   => false,
                    'background-image'      => false,
                    'output'                => array('.header-top-area'),
                    'title'                 => esc_html__('Top Bar Background Color', 'moto'),
                    'subtitle'              => esc_html__('Controls the background color of the header top bar area( default #1E2127 ).', 'moto'),
                    'default'               => array(
                        
                    )
                ),
                 array(
                    'id'                    => 'moto_top_text_colors',
                    'type'                  => 'color',
                    'output'                => array('.top-bar-left-content,.header-info' ),
                    'title'                 => esc_html__('Top Bar Text Color', 'moto'),
                    'subtitle'              => esc_html__('Controls the color of the top bar text.', 'moto'),
                    'transparent'           => false,
                    
                ),
                array(
                    'id'                    => 'moto_top_link_colors',
                    'type'                  => 'color',
                    'output'                => array('.header-info span a, .header-social ul li a, .top-bar-left-content p a, .top-bar-left-menu ul li a, .header-social ul li a' ),
                    'title'                 => esc_html__('Top Bar Link Color ', 'moto'),
                    'subtitle'              => esc_html__('Controls the link color of the top bar.', 'moto'),
                    'transparent'           => false,
                   
                ),
                array(
                    'id'                    => 'moto_top_link_colors_hover',
                    'type'                  => 'color',
                    'output'                => array('.header-info span a:hover, .header-social ul li a:hover, .top-bar-left-content p a:hover'),
                    'title'                 => esc_html__('Top Bar Link Hover Color', 'moto'),
                    'subtitle'              => esc_html__('Controls the link hover color of the top bar.', 'moto'),
                    'transparent'           => false,
                ),
                array(
                    'id'                    => 'moto_top_bar_padding',
                    'type'                  => 'spacing',
                    'mode'                  => 'padding',
                    'title'                 => esc_html__('Top Bar padding ', 'moto'),
                    'subtitle'              => esc_html__('Controls header Top Bar padding.', 'moto'),
                    'right'                 => false,
                    'left'                  => false,
                    'output'                => array('.header-top-area'),
                    'units'                 => array('em','px'),
                    'default'               => array(
                        'padding-top'       => 'px',
                        'padding-right'     => '0px',
                        'padding-bottom'    => 'px',
                        'padding-left'      => '0px',
                        'units'             => 'px',
                    ),
                ),
            )
        ) 
    );
    //Main menu
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Menu', 'moto' ),
        'id'               => 'moto-header-contact-top',
        'icon'             => 'el el-lines',
        'customizer_width' => '500px',
        'fields'           => array(
                
            )
        ) 
    );
    //Menu tryphograpy
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Menu Typography', 'moto' ),
        'id'               => 'moto_menu_typography',
        'icon'             => 'el el-arrow-right',
        'subsection'       => 'true',
        'customizer_width' => '500px',
        'fields'           => array(
                array(
                    'id'                    => 'moto_menufont',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Menu Font Settings', 'moto'),
                    'google'                => true,     
                    'subsets'               => false,
                    'line-height'           => false,
                    'text-transform'        => true,
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('.primary-nav-wrap ul li a'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('Controls the font settings of the menu text', 'moto'),
                    'default'               => array(
                        'google'            => true,
                        'font-family'       => 'Open Sans',
                        'font-weight'       => '600',
                        'text-transform'    => 'capitalize',
                        'font-size'       => '16px',
                        'color'             => '#ffffff',
                    )
                ),
                array(
                    'id'                    => 'moto_menu_hover_colors',
                    'type'                  => 'color',
                    'output'                => array('.primary-nav-wrap > nav > ul > li:hover > a, .primary-nav-wrap > nav > ul > li.current_page_item > a' ),
                    'title'                 => esc_html__('Menu Hover Color', 'moto'),
                    'subtitle'              => esc_html__('Controls the menu hover color of the menu text(default: #333).', 'moto'),
                    'transparent'           => false,
                    'validate'              => 'color',
                ),
               
               array(
                    'id'                    => 'moto_main_menu_padding',
                    'type'                  => 'spacing',
                    'title'                 => esc_html__( 'Main Menu Padding', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the top and bottom padding of the main menu', 'moto' ),
                    'output'                => array('.primary-nav-wrap nav .menu > li'),
                    'mode'                  => 'padding',
                    'right'                 => false,
                    'left'                  => false,
                    'units_extended'        => 'false',
                    'units'                 => array('em','px'),
                    'default'               => array(
                        'padding-top'       => 'px',
                        'padding-right'     => '0px',
                        'padding-bottom'    => 'px',
                        'padding-left'      => '0px',
                        'units'             => 'px',
                        )
                ),
                array(
                    'id'                    => 'moto_menu_item_spacing',
                    'type'                  => 'text',
                    'title'                 => esc_html__( 'Main Menu Item Spacing', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the menu item spacing of the main menu', 'moto' ),    
                ),
               

            )
        ) 
    );
    //Menu tryphograpy
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Dropdown menu', 'moto' ),
        'id'               => 'moto_menu_dropdown',
        'icon'             => 'el el-arrow-right',
        'subsection'       => 'true',
        'customizer_width' => '500px',
        'fields'           => array( 
             array(
                    'id'                    => 'moto_submenufont',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Submenu Font Settings', 'moto'),
                    'google'                => true,       
                    'subsets'               => false,
                    'line-height'           => false,
                    'transition'            => false,
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'text-transform'        => true,    
                    'output'                => array('.primary-nav-wrap .sub-menu > li > a'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('Controls the font settings of the submenu text', 'moto'),
                    'default'               => array(
                        'google'            => true,
                        'font-family'       => 'Open Sans',
                        'font-weight'       => '400',
                        'text-transform'    => 'capitalize',
                        'font-size'       => '14px',
                        'color'             => '#ffffff',
                    )
                ),
                array(
                    'id'                    => 'moto_submenu_hover_colors',
                    'type'                  => 'color',
                    'output'                => array('.primary-nav-wrap .sub-menu > li:hover > a' ),
                    'title'                 => esc_html__('Submenu Hover Color', 'moto'),
                    'subtitle'              => esc_html__('Controls the submenu hover color(default: #333).', 'moto'),
                    'transparent'           => false,
                    'validate'              => 'color',
                ),

                array(
                    'id'                    => 'moto_submenu_background_color',
                    'type'                  => 'color',
                    'title'                 => esc_html__('Submenu Background', 'moto'),
                    'subtitle'              => esc_html__('Controls the background color of the submenu', 'moto'),
                ),
                 array(
                    'id'                    => 'moto_bropdown_bg_hover',
                    'type'                  => 'background',
                    'background-attachment' => false,
                    'background-repeat'     => false,
                    'background-size'       => false,
                    'background-position'   => false,
                    'background-image'      => false,
                    'output'                => array('.primary-nav-wrap .sub-menu > li:hover > a'),
                    'title'                 => esc_html__('Submenu Hover Background.', 'moto'),
                    'subtitle'              => esc_html__('Controls the hover background color of the submenu( default #333 ).', 'moto'),
                ),

                 array(
                    'id'                    => 'moto_menu_dropdowshowh_color',
                    'output'                => array('.primary-nav-wrap ul.sub-menu li + li'),
                    'type'                 => 'border',
                    'title'             => esc_html__('Submenu Separator Color', 'moto'),
                    'subtitle'          => esc_html__('Controls the separator color of the submenu', 'moto'),
                    'output'            => array('.primary-nav-wrap ul.sub-menu li + li'),
                    'right'             => false,
                    'bottom'            => false,
                    'left'              => false,
                ),
                 array(
                    'id'                    => 'moto_menu_dropdownwidth',
                    'type'                  => 'text',
                    'title'                 => esc_html__( 'Submenu Width', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the submenu width', 'moto' ),
                ),
                  array(
                    'id'                    => 'moto_dropdown_item_padding',
                    'type'                  => 'text',
                    'title'                 => esc_html__( 'Submenu Item Height', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the height of the submenu item', 'moto' ),
                ),

            )
        ) 
    );
    //Sticky Header Menu
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Sticky Menu', 'moto' ),
        'id'               => 'moto_sticky_menu_typography',
        'icon'             => 'el el-arrow-right',
        'subsection'       => 'true',
        'customizer_width' => '500px',
        'fields'           => array(
             array(
                    'id'                    => 'moto_menu_link_colors',
                    'type'                  => 'color',
                    'output'                => array('.header-area.header-sticky.sticky .primary-nav-wrap ul > li > a' ),
                    'title'                 => esc_html__( 'Sticky Header Menu Text Color', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the header menu text color when the header is sticky', 'moto' ),
                    'transparent'           => false,
                ),
                array(
                    'id'                    => 'moto_header_sticky_bg',
                    'type'                  => 'background',
                    'background-attachment' => false,
                    'background-repeat'     => false,
                    'background-size'       => false,
                    'background-position'   => false,
                    'background-image'      => false,
                    'output'                => array('.header-area.sticky'),
                    'title'                 => esc_html__( 'Sticky Header Background color', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the header background color when the header is sticky( default ).', 'moto' ),
                ),
                array(
                    'id'                    => 'moto_menu_sticky_font',
                    'type'                  => 'color',
                    'output'                => array('.header-sticky.sticky .site-title a' ),
                    'title'                 => esc_html__( 'Sticky Header Logo Text Color', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the sticky header logo text color when the header is sticky', 'moto' ),
                    'transparent'           => false,
                ),

            )
        ) 
    );
    //Mobile Menu Visibility
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Mobile Menu', 'moto' ),
        'id'               => 'moto_Mobile_menu',
        'icon'             => 'el el-arrow-right',
        'subsection'       => 'true',
        'customizer_width' => '500px',
        'fields'           => array(
                array(
                    'id'                    => 'moto_mobile_menu_width',
                    'type'                  => 'text',
                    'title'                 => esc_html__( 'Display Mobile Menu at', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls in which screen size the mobile menu will be displayed( ex. 991px )', 'moto' ),
                ),
            )
        ) 
    );

    //logo
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Logo', 'moto' ),
        'id'               => 'moto_logo',
        'customizer_width' => '400px',
        'icon'             => 'el el-plus-sign',
        'fields'           => array(
                array(
                    'id'                    => 'moto_main_logo',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__('Enable / Disable Logo','moto'),
                    'desc'                  => esc_html__('By enabling the logo you will be able to set a logo in the header and by disabling the logo you will be able to set site title in the header.','moto'),
                    'options'               => array(
                        '1'                 => 'Enable',
                        '2'                 => 'Disable',
                    ),
                    'default'               => '1'
                ),
                array(
                    'id'                    => 'moto_head_logo',
                    'type'                  => 'media',
                    'required'              => array('moto_main_logo','equals','1'),
                    'title'                 => esc_html__('Logo','moto'),
                    'desc'                  => esc_html__('Upload your logo here','moto'),
                    'default'               => array(
                        'url'               => get_template_directory_uri().'/images/logo.png'
                    ),
                ),
                array(
                    'id'                    => 'moto_logo_text',
                    'type'                  => 'text',
                    'required'              => array('moto_main_logo','equals','2'),
                    'title'                 => esc_html__('Site Title','moto'),
                    'desc'                  => esc_html__('Enter your site title here','moto'),
                    'default'               => get_bloginfo('name')
                ),
                array(
                    'id'                    => 'moto_logo_text_font',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Site Title Font Settings', 'moto'),
                    'required'              => array('moto_main_logo','equals','2'),
                    'google'                => true,     
                    'subsets'               => false,
                    'line-height'           => false,
                    'text-transform'        => true,
                    'transition'            => false,
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('.site-title a'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('Controls the font settings of the site title', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
            )
        ) 
    );

    //Page Title Bar
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Page Title', 'moto' ),
        'id'               => 'moto_breadcrumbs',
        'customizer_width' => '400px',
        'icon'             => 'el el-adjust-alt',
        'fields'           => array(
                array(
                    'id'                    => 'moto_page_title_bar',
                    'title'                 => esc_html__('Show Page Title Area','moto'),
                    'subtitle'              => esc_html__('Show or Hide the page title area from here','moto'),
                    'type'                  => 'button_set',
                    'options'               => array(
                        '1'                 => esc_html__('Show','moto'),
                        '2'                 => esc_html__('Hide','moto'),
                    ),
                    'default'               => '1',
                ),
                array(
                    'id'                    => 'moto_breadcrumbs_area',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Show Page Title', 'moto' ),
                    'subtitle'              => esc_html__( 'Show or Hide the page title from here', 'moto' ),
                    'default'               => true,
                ),
                array(
                    'id'                    => 'moto_page_title_colors',
                    'type'                  => 'typography',
                   'title'                  => esc_html__('Page Title Font Settings', 'moto'),
                    'google'                => true,     
                    'subsets'               => false,
                    'line-height'           => false,
                    'text-transform'        => true,
                    'transition'            => false,
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('.breadcrumbs h2.page-title'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('Controls the font settings of page title', 'moto'),
                    'default'               => array(
                        'google'            => true,
                        'font-family'       => 'Open Sans',
                        'font-weight'       => '600',
                        'text-transform'    => 'capitalize',
                        'font-family'       => '14px'
                    )
                ), 
                array(
                    'id'                    => 'moto_breadcrumbs_text',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__('Page Title Text Alignment','moto'),
                    'subtitle'              => esc_html__('Controls the text alignment settings of the page title','moto'),
                    'options'               => array(
                        'left'              => esc_html__('left','moto'),
                        'center'            => esc_html__('center','moto'),
                        'right'             => esc_html__('right','moto'),
                    ), 
                    'default'               => 'left'
                ),
                array(
                    'id'                    => 'moto_breadcrumbs_full',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Full Width Page Title', 'moto' ),
                    'subtitle'              => esc_html__( 'Turn on to have the page title area display at 100% width according to the window size. Turn off to follow site width.', 'moto' ),
                    'default'               => false,
                ),
                array(
                    'id'                    => 'moto_breadcrumb_lg_height',
                    'type'                  => 'spacing',
                    'title'                 => esc_html__( 'Page Title Area Padding', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the height of the page title area. Enter value excluding any valid CSS unit, ex: 100', 'moto' ),
                    'output'                => array(''),
                    'mode'                  => 'padding',
                    'left'                  => false,
                    'right'                 => false,
                    'units_extended'        => 'false',
                    'default'               => array(
                        'padding-top'       => 'px',
                        'padding-right'     => '0px',
                        'padding-bottom'    => 'px',
                        'padding-left'      => '0px',
                        )
                ),
                array(
                    'id'                    => 'moto_breadcrumbs_mobile_height',
                    'type'                  => 'spacing',
                    'title'                 => esc_html__( 'Page Title Area Padding on Mobile', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the height of the page title area on mobile device. Enter value excluding any valid CSS unit, ex: 80', 'moto' ),
                    'output'                => array(''),
                    'mode'                  => 'padding',
                    'left'                  => false,
                    'right'                 => false,
                    'units_extended'        => 'false',
                    'units'                 => array('em','px'),
                    'default'               => array(
                        'padding-top'       => 'px',
                        'padding-right'     => '0px',
                        'padding-bottom'    => 'px',
                        'padding-left'      => '0px',
                        'units'             => 'px',
                        )
                ),
                array(
                    'id'                    => 'moto_breadcrumbs_bg_optins',
                    'type'                  => 'background',
                    'output'                => array(' '),
                    'title'                 => esc_html__('Background Color', 'moto'),
                    'subtitle'              => esc_html__('Controls the background color of the page title area.', 'moto'),
                     'default'               => array(
                        'background-position'       => 'center center',
                        'background-repeat'         => 'no-repeat',
                        'background-size'           => 'cover',
                        'background-image'          => get_template_directory_uri().'/images/breacrumb-bg.png'
                    )
                ),
                array(
                    'id'                    => 'moto_breadcrumbs_overlay_color',
                    'type'                  => 'color',
                    'output'                => array('' ),
                   'title'                  => esc_html__('Overlay Color', 'moto'),
                    'subtitle'              => esc_html__('Controls the overlay color of the page title area.', 'moto'),
                    'transparent'           => false,
                    'validate'              => 'color',
                    // 'default'               => '#333'
                ),
                array(
                    'id'                    => 'moto_breadcrumbs_overlay_color_opacity',
                    'type'                  => 'slider',
                    'title'                 => esc_html__('Overlay Opacity', 'moto'),
                    'subtitle'              => esc_html__('Controls the opacity of the overlay color', 'moto'),
                    'desc'                  => esc_html__('Overlay opacity. Min: 0, max: 1, step: .1, default value: .8', 'moto'),
                    "default"               => '.8',
                    "min"                   => 0,
                    "step"                  => .1,
                    "max"                   => 1,
                    'resolution'            => 0.1,
                    'display_value'         => 'text',
                )
            )
        ) 
    );
    //breadcrumbs
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Breadcrumb','moto'),
        'icon'      => 'el el-arrow-right',
        'id'        => 'moto_blog_breadcrumb',
        'subsection'=> true,
        'fields'    => array(
                array(
                    'id'                    => 'moto_breadcrumbs_content_blog',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__( 'Show Breadcrumb Content', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls what displays in the breadcrumb area.', 'moto' ),
                    'options'               => array (
                        '1'                 => esc_html__('None','moto'),
                        '2'                 => esc_html__('Breadcrumbs','moto'),
                        '3'                 => esc_html__('Search Box','moto'),
                    ),
                    'default'               => '2',
                ),
                array(
                    'id'                    => 'moto_breadcrumbs_mobile',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Breadcrumb on Mobile Devices', 'moto' ),
                    'subtitle'              => esc_html__( 'Turn on to display breadcrumb on mobile devices.', 'moto' ),
                    'default'               => true,
                ),

                array(
                    'id'                    => 'moto_breadcrumbs_separator',
                    'type'                  => 'text',
                    'title'                 => esc_html__('Breadcrumb Separator','moto'),
                    'subtitle'              => esc_html__('Set the breadcrumb separator here','moto'),
                    'default'               => ' / '
                ),

                array(
                    'id'                    => 'moto_breadcrumbs_font_blog',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Breadcrumb Font Settings', 'moto'),
                    'google'                => true,      
                    'subsets'               => false,
                    'line-height'           => false,
                    'transition'            => false,
                    'text-align'            => false,
                    'font-style'            => false,    
                    'font-family'           => false,    
                    'font-family'           => false,    
                    'font-weight'           => false,    
                    'output'                => array('.breadcrumbs ul li a,.breadcrumbs ul, .breadcrumbs ul li'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('Controls the font settings of the breadcrumb.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_breadcrumbs_font_size',
                    'type'                  => 'text',
                    'output'                => array(''),
                    'title'                 => esc_html__('Breadcrumb Text Size', 'moto'),
                    'subtitle'              => esc_html__('Controls the size of the breadcrumb text.', 'moto'),
                 ),
                array(
                    'id'                    => 'moto_breadcrumbs_font_blog',
                    'type'                  => 'color',
                    'output'                => array(''),
                    'title'                 => esc_html__('Breadcrumb Text Color', 'moto'),
                    'subtitle'              => esc_html__('Controls breadcrumb text color.', 'moto'),
                 ),
                array(
                    'id'                    => 'moto_breadcrumbs_text_hover_blog',
                    'type'                  => 'color',
                    'output'                => array(''),
                    'title'                 => esc_html__('Breadcrumb Text Hover Color', 'moto'),
                    'subtitle'              => esc_html__('Controls breadcrumb text hover color.', 'moto'),
                 ),

                array(
                    'id'                    => 'moto_breadcrumbs_post_cat',
                    'type'                  => 'switch',
                    'title'                 => esc_html__('Show Post Categories on Breadcrumb', 'moto'),
                    'subtitle'              => esc_html__('Turn on to display the post categories in the breadcrumb path.', 'moto'),
                    'default'               => true,
                 ),
                
                
            )
        )
    );
     //Header center
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__('Social Icon', 'moto'),
        'id'               => 'moto-social-icon',
        'icon'             => 'el el-arrow-right',      
        'fields'           => array( 
                array(
                    'id'                    => 'moto_social_icons',
                    'type'                  => 'sortable',
                    'title'                 => esc_html__('Social Icons', 'moto'),
                    'subtitle'              => esc_html__('Enter social links to show the icons', 'moto'),
                    'mode'                  => 'text',
                    'label'                 => true,
                    'options'               => array(        
                        'facebook'          => '',
                        'twitter'           => '',
                        'instagram'         => '',
                        'tumblr'            => '',
                        'pinterest'         => '',
                        'google-plus'       => '',
                        'linkedin'          => '',
                        'behance'           => '',
                        'dribbble'          => '',
                        'youtube'           => '',
                        'vimeo'             => '',
                        'rss'               => '',
                ),
                'default'                   => array(
                    'facebook'              => 'https://www.facebook.com/',
                    'twitter'               => 'https://twitter.com/',
                    'instagram'             => 'https://instagram.com/',
                    'tumblr'                => '',
                    'pinterest'             => '',
                    'google-plus'           => 'https://plus.google.com/',
                    'linkedin'              => '',
                    'behance'               => '',
                    'dribbble'              => 'https://dribbble.com/',
                    'youtube'               => '',
                    'vimeo'                 => '',
                    'rss'                   => '',
                ),
            ))
        ) 
    );
    //Fonts
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Typography', 'moto'),
        'id'        => 'moto_fonts',
        'icon'      => 'el el-fontsize',
        'fields'    => array(
                array(
                    'id'                    => 'moto_bodyfont',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Body Typography', 'moto'),
                    'google'                => true,        
                    'subsets'               => false, 
                    'word-spacing'          => true, 
                    'letter-spacing'        => true,
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('body'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('Controls the typography for all body font settings.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_bodyfont_link',
                    'type'                  => 'color',
                    'output'                => array( 'a'),
                    'title'                 => esc_html__('Link Color','moto'),
                    'subtitle'              => esc_html__('Controls the color of all links.','moto'),
                ),
                array(
                    'id'                    => 'moto_bodyfont_link_hover',
                    'type'                  => 'color',
                    'output'                => array( 'a:hover, a:focus, a:active'),
                    'title'                 => esc_html__('Link Color Hover','moto'),
                    'subtitle'              => esc_html__('Controls the hover color of all links.','moto'),
                ),
            )
        ) 
    );

    //Heading font
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Heading Typography', 'moto'),
        'id'        => 'moto_headingfont',
        'icon'      => 'el el-arrow-right',
        'subsection'=> true,
        'fields'    => array(
                array(
                    'id'                    => 'moto_headingfonth1',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Heading H1 Typography', 'moto'),
                    'google'                => true,    
                    'text-transform'        => true, 
                    'word-spacing'          => true, 
                    'letter-spacing'        => true,                    
                    'subsets'               => false, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('h1'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('These settings control the typography for all H1 Headings.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_headingfonth2',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Heading H2 Typography', 'moto'),
                    'google'                => true,  
                    'text-transform'        => true, 
                    'letter-spacing'        => true,                    
                    'word-spacing'          => true,    
                    'subsets'               => false, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('h2'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('These settings control the typography for all H2 Headings.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_headingfonth3',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Heading H3 Typography', 'moto'),
                    'google'                => true, 
                    'text-transform'        => true, 
                    'letter-spacing'        => true,                    
                    'word-spacing'          => true,    
                    'subsets'               => false, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('h3'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('These settings control the typography for all H3 Headings.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_headingfonth4',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Heading H4 Typography', 'moto'),
                    'google'                => true,    
                    'text-transform'        => true, 
                    'word-spacing'          => true, 
                    'letter-spacing'        => true,                    
                    'subsets'               => false, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('h4'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('These settings control the typography for all H4 Headings.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_headingfonth5',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Heading H5 Typography', 'moto'),
                    'google'                => true,    
                    'text-transform'        => true, 
                    'word-spacing'          => true, 
                    'letter-spacing'        => true,                    
                    'subsets'               => false, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('h5'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('These settings control the typography for all H5 Headings.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_headingfonth6',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Heading H6 Typography', 'moto'),
                    'google'                => true,    
                    'text-transform'        => true,  
                    'word-spacing'          => true, 
                    'letter-spacing'        => true,                    
                    'subsets'               => false, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('h6'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('These settings control the typography for all H6 Headings.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
        )
    ) );
    // Blog options
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Blog', 'moto'),
        'icon'      => 'el el-file-edit',
        'id'        => 'moto_blog',
        'fields'    => array(
                array(
                    'id'                    => 'moto_bolg_title_bar',
                    'type'                  => 'switch',
                    'title'                 => esc_html__('Page Title Area', 'moto'),
                    'subtitle'              => esc_html__('Turn on to show the page title area for the assigned blog page in "settings > reading" or blog archive pages.','moto'),
                    'default'               => true,
                ),
                array(
                    'id'                    => 'moto_readmore_text',
                    'type'                  => 'text',
                    'title'                 => esc_html__('Read More Text', 'moto'),
                    'default'               => esc_html__('Read more','moto')
                ),
                array(
                    'id'                    => 'moto_excerpt_length',
                    'type'                  => 'slider',
                    'title'                 => esc_html__('Excerpt Length On Blog Page', 'moto'),
                    'subtitle'              => esc_html__('Controls the excerpt length on blog page','moto'),
                    "default"               => 30,
                    "min"                   => 10,
                    "step"                  => 2,
                    "max"                   => 130,
                    'display_value'         => 'text'
                ),
                array(
                    'id'                    => 'moto_sidebarblog_pos',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__('Blog Style', 'moto'),
                    'subtitle'              => esc_html__('Choose your blog style', 'moto'),
                    'desc'                  => esc_html__('Select blog page style', 'moto'),
                    'options'               => array(
                        'single'            => esc_html__('Single Column','moto'),
                        'twocolumn'         => esc_html__('Blog two column','moto'),
                        'threecolumn'       => esc_html__('Blog three colum','moto'),
                        'left'              => esc_html__('Blog Left sidebar','moto'),
                        'right'             => esc_html__('Blog Right sidebar','moto'),
                    ),
                    'default'               => 'right',
                ),
                array(
                    'id'                    => 'moto_blog_pheights',
                    'type'                  => 'spacing',
                    'title'                 => esc_html__( 'Blog Page Padding.', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the top and bottom padding of the blog page', 'moto' ),
                    'output'                => array('.our-blog-area'),
                    'mode'                  => 'padding',
                    'units_extended'        => 'false',
                    'left'                  => 'false',
                    'right'                 => 'false',
                    'units'                 => array('em','px'),
                    'default'               => array(
                        'padding-top'       => 'px',
                        'padding-right'     => '0px',
                        'padding-bottom'    => 'px',
                        'padding-left'      => '0px',
                        'units'             => 'px',
                        )
                ),


            )
        ) 
    );  
    // Blog archive
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Blog Page Title', 'moto'),
        'icon'      => 'el el-arrow-right',
        'id'        => 'moto_blog_id',
        'subsection'=> true,
        'fields'    => array(           
                array(
                    'id'                    => 'moto_blog_text',
                    'type'                  => 'text',
                    'title'                 => esc_html__('Blog Page Title', 'moto'),
                    'subtitle'              => esc_html__('Controls the title text that displays in the page title area of the assigned blog page. This option only works if your front page displays your latest post in "settings > reading" or blog archive pages.','moto'),
                    'default'               => 'Blog',
                ),
                array(
                    'id'                    => 'moto_bolg_subtitle',
                    'type'                  => 'text',
                    'title'                 => esc_html__('Blog Page Subtitle', 'moto'),
                    'subtitle'              => esc_html__('Controls the subtitle text that displays in the page title area of the assigned blog page. This option only works if your front page displays your latest post in "settings > reading" or blog archive pages.','moto'),
                ),
                array(
                    'id'                    => 'moto_bolg_title_color',
                    'type'                  => 'color',
                    'output'                => array('.bredcrumb-blog h1, .bredcrumb-blog h3 '),
                    'title'                 => esc_html__('Blog Title Color', 'moto'),
                    'subtitle'              => esc_html__('Controls blog title color.','moto'),
                ),
                array(
                    'id'                    => 'moto_bolg_title_position',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__('Blog Title Position', 'moto'),
                    'subtitle'              => esc_html__('Controls the position of the blog title','moto'),
                    'options'               => array(
                        'left'              => esc_html__('Left','moto'),
                        'center'            => esc_html__('Center','moto'),
                        'right'             => esc_html__('Right','moto'),
                    ),
                    'default'               => 'center',
                ),
                array(
                    'id'                    => 'moto_blog_banner',
                    'type'                  => 'background',
                    'output'                => array('.bredcrumb-blog'),
                    'title'                 => esc_html__('Blog Page Title Banner', 'moto'),
                     'default'               => array(
                        'background-position'       => 'center center',
                        'background-repeat'         => 'no-repeat',
                        'background-size'           => 'cover',
                        'background-image'          => get_template_directory_uri().'/images/breacrumb-bg.png'
                    )
                ),
            )
        ) 
    );  
    // Single blog
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Blog Details', 'moto'),
        'icon'      => 'el el-arrow-right',
        'id'        => 'moto_blog_single',
        'subsection'=> true,
        'fields'    => array(
                array(
                    'id'                    => 'moto_single_pos',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__('Blog Details Layout', 'moto'),
                    'subtitle'              => esc_html__('Choose your favorite style', 'moto'),
                    'options'               => array(
                        'full'              => esc_html__('Full Width','moto'),
                        'left'              => esc_html__('Left Sidebar','moto'),
                        'right'             => esc_html__('Right sidebar','moto'),
                    ),
                    'default'               => 'right',
                ),
                array(
                    'id'                    => 'moto_blog_spheights',
                    'type'                  => 'spacing',
                    'title'                 => esc_html__( 'Blog Details Page Padding.', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the top and bottom padding of the single blog page', 'moto' ),
                    'output'                => array('.blog-story-area'),
                    'mode'                  => 'padding',
                    'units_extended'        => 'false',
                    'left'                  => 'false',
                    'right'                 => 'false',
                    'units'                 => array('em','px'),
                    'default'               => array(
                        'padding-top'       => 'px',
                        'padding-right'     => '0px',
                        'padding-bottom'    => 'px',
                        'padding-left'      => '0px',
                        'units'             => 'px',
                        )
                ),

                array(
                    'id'                    => 'moto_blog_details_show_title',
                    'type'                  => 'switch',
                    'title'                 => esc_html__('Post Title', 'moto'),
                    'default'               => false,
                ),
                array(
                    'id'                    => 'moto_blog_details_show_meta',
                    'type'                  => 'switch',
                    'title'                 => esc_html__('Post top Meta', 'moto'),
                    'default'               => false,
                ),
                array(
                    'id'                    => 'moto_blog_details_social_share',
                    'type'                  => 'switch',
                    'title'                 => esc_html__('Social Share', 'moto'),
                    'default'               => false,
                ),
                array(
                    'id'                    => 'moto_blog_details_post_navigation',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Post Navigation (Next/Previous)', 'moto' ),
                    'default'               => true,
                ),
                array(
                    'id'                    => 'moto_blog_details_show_author_info',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Show Author Info', 'moto' ),
                    'default'               => true,
                ),
                array(
                    'id'                    => 'moto_blog_details_show_related_post',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Show Related Post', 'moto' ),
                    'default'               => true,
                ),
                array(
                    'id'                    => 'moto_blog_details_related_post_title',
                    'type'                  => 'text',
                    'title'                 => esc_html__( 'Related Post Title', 'moto' ),
                    'default'               => esc_html__( 'Related Post', 'moto' ),
                ),
                array(
                    'id'                    => 'moto_blog_details_no_of_column_related_post',
                    'type'                  => 'select',
                    'title'                 => esc_html__( 'No. of Column Related post', 'moto' ),
                        'options'  => array(
                            '12' => '1 Column',
                            '1' => '12 Column',
                            '2' => '6 Column',
                            '3' => '4 Column',
                            '4' => '3 Column',
                            '6' => '2 Column',
                        ),
                    'default'               => 4,
                ),
                array(
                    'id'        => 'moto_blog_details_no_of_item_per_page',
                    'type'      => 'slider',
                    'title'     => esc_html__( 'No. of Item per page', 'moto' ),
                    'desc'      => esc_html__('Min: -1, max: --, step: 1, default value: 3 (if you want to show all post then set -1 value.)', 'moto'),
                    "default"   => 3,
                    "min"       => -1,
                    "step"      => 1,
                    "max"       => 20,
                    'display_value' => 'text'
                ),


                
            )
    ) );
    // Sidebar
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Sidebar', 'moto'),
        'icon'      => 'el el-arrow-right',
        'id'        => 'moto_sidebar',
        'subsection'=> true,
        'fields'    => array(

                array(
                    'id'                    => 'moto_sidebar_widget_title',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Widget Title', 'moto'),
                    'subtitle'              => esc_html__('Controls the settings of the widget title', 'moto'),
                    'google'                => true,       
                    'subsets'               => false, 
                    'text-transform'        => true, 
                    'letter-spacing'        => true, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('.sidebar-title'), 
                    'units'                 => 'px',
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_sidebar_widget_title',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Widget Title', 'moto'),
                    'subtitle'              => esc_html__('Controls the settings of the widget title', 'moto'),
                    'google'                => true,       
                    'subsets'               => false, 
                    'text-transform'        => true, 
                    'letter-spacing'        => true, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('.sidebar-title'), 
                    'units'                 => 'px',
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                        => 'moto_sidebar_widget_title_spacing',
                    'type'                      => 'spacing',
                    'title'                     => esc_html__( 'Widget Title Spacing', 'moto' ),
                    'output'                    => array('.sidebar-title:not(.related-post-title)'),
                    'mode'                      => 'margin',
                    'units_extended'            => 'false',
                    'units'                     => array('em','px'),
                    'default'                   => array(
                        'margin-top'            => 'px',
                        'margin-right'          => 'px',
                        'margin-bottom'         => 'px',
                        'margin-left'           => 'px',
                        'units'                 => 'px',
                    )
                ),
                array(
                    'id'                    => 'moto_sidebar_widget_title_separetor',
                    'type'                  => 'background',
                    'background-attachment' => false,
                    'background-size'       => false,
                    'background-position'   => false,
                    'background-image'      => false,
                    'background-repeat'     => false,
                    'output'                => array( '.sidebar-title::before' ),
                    'title'                     => esc_html__( 'Widget Title Separator color', 'moto' ),
                    'subtitle'                  => esc_html__( 'Controls the Separator color.', 'moto' ),
                ),
                

                
            )
    ) );

    //woocommerce
    if(class_exists('woocommerce')){
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__('WooCommerce', 'moto'),
        'desc'       => esc_html__('WooCommerce options', 'moto'),
        'icon'       => 'el el-shopping-cart',      
        'fields'     => array(
    )) 
    );

         // Product Listing
    Redux::setSection( $opt_name, array(
        'title'        => esc_html__('Product Listing Setting', 'moto'),
        'desc'         => esc_html__('Use this section to select options for product.', 'moto'),
        'icon'         => 'el-icon-tags',
        'subsection'   => true,      
        'fields'                            => array(
            array(
                'id'                       => 'product_per_page',
                'type'                     => 'slider',
                'title'                    => esc_html__('Products per page', 'moto'),
                'subtitle'                 => esc_html__('Amount of products per page on shop page.', 'moto'),
                "default"                  => 12,
                "min"                      => 3,
                "step"                     => 1,
                "max"                      => 48,
                'display_value'            => 'text'
            ),

            array(
                'id'                    => 'moto_shop_page_layout',
                'title'                 => esc_html__('Select Shop page layout', 'moto'),
                'subtitle'              => esc_html__('Select Shop page layout.', 'moto'),
                'type'                  => 'image_select',
                'options'               => array(
                    'left'                 => array(
                        'title'         => esc_html__('Left Sidebar', 'moto'),
                        'img'           => get_template_directory_uri().'/images/optionframework/left-sidebar.png',
                    ),
                    'fullw'                 => array(
                        'title'         => esc_html__(' Full widht  ', 'moto'),
                        'img'           => get_template_directory_uri().'/images/optionframework/no-sidebar.png',
                    ),
                    'right'                 => array(
                        'title'         => esc_html__(' Right Sidebar', 'moto'),
                        'img'           => get_template_directory_uri().'/images/optionframework/right-sidebar.png',
                    ),
                ),
                'default'               => 'fullw'
            ),

        )) 
    );

     //Woocommerce single page
        Redux::setSection( $opt_name, array(
        'title'            => esc_html__('WooCommerce Single Page', 'moto'),
        'id'               => 'moto_woo_single',
        'icon'             => 'el el-arrow-right',
        'subsection'       => true,  
        'fields'           => array(                
               array(
                'id'                    => 'moto_shop_single_page_layout',
                'title'                 => esc_html__('Select Single Shop page layout', 'moto'),
                'subtitle'              => esc_html__('Select single Shop page layout.', 'moto'),
                'type'                  => 'image_select',
                'options'               => array(
                    'left'              => array(
                        'title'         => esc_html__('Left Sidebr', 'moto'),
                        'img'           => get_template_directory_uri().'/images/optionframework/left-sidebar.png',
                    ),
                    'fullw'             => array(
                        'title'         => esc_html__('Full Width ', 'moto'),
                        'img'           => get_template_directory_uri().'/images/optionframework/no-sidebar.png',
                    ),
                    'right'             => array(
                        'title'         => esc_html__('Right Sidebar ', 'moto'),
                        'img'           => get_template_directory_uri().'/images/optionframework/right-sidebar.png',
                    ),
                ),
                'default'               => 'fullw'
            ),
            
       
            
            )
        ));
        
    }


    //footer section
    Redux::setSection($opt_name, array(
        'title'   => esc_html__('Footer setting','moto'),
        'id'      => 'moto_footer_section',
        'icon'    => 'el el-brush',
        'fields'  => array(
            array(
                'id'                        => 'moto_footer_widget',
                'type'                      => 'switch',
                'title'                     => esc_html__( 'Show Footer Widgets Area', 'moto' ),
                'subtitle'                  => esc_html__( 'Turn on to display footer widgets area','moto' ),
                'default'                   => false,
            ),
            array(
                'id'                        => 'moto_footer_layoutcolumns',
                'title'                     => esc_html__('Number of Footer Columns','moto'),
                'subtitle'                  => esc_html__( 'Controls the number of columns in the footer.', 'moto' ),
                'desc'                      => esc_html__( 'Select the number of columns', 'moto' ),
                'type'                      => 'button_set',
                'options'                   => array(
                    '1'                     => esc_html__('One Column','moto'),
                    '2'                     => esc_html__('Two Column','moto'),
                    '3'                     => esc_html__('Three Column','moto'),
                    '4'                     => esc_html__('Four Column','moto'),
                ),
                'default'                   => '4'
            ),
            array(
                'id'                        => 'moto_footer_bg_color',
                'type'                      => 'background',
                'output'                    => array( '.footer-top-area ' ),
                'title'                     => esc_html__( 'Footer Area Background Color', 'moto' ),
                'subtitle'                  => esc_html__( 'Control the background color of the footer area(default: #252830).', 'moto' ),
            ),
            array(
                    'id'                    => 'moto_footer_overlay_color',
                    'type'                  => 'color',
                    'output'                => array('' ),
                   'title'                  => esc_html__('Overlay Color', 'moto'),
                    'subtitle'              => esc_html__('Controls the overlay color of the footer area.', 'moto'),
                    'transparent'           => false,
                    'validate'              => 'color',
            ),
            array(
                    'id'                    => 'moto_footer_overlay_color_opacity',
                    'type'                  => 'slider',
                    'title'                 => esc_html__('Overlay Opacity', 'moto'),
                    'subtitle'              => esc_html__('Controls the opacity of the overlay color', 'moto'),
                    'desc'                  => esc_html__('Overlay opacity. Min: 0, max: 1, step: .1, default value: .8', 'moto'),
                    "default"               => .8,
                    "min"                   => 0,
                    "step"                  => .1,
                    "max"                   => 1,
                    'resolution'            => 0.1,
                    'display_value'         => 'text'
            ),
            array(
                'id'                        => 'moto_footer_widget_width',
                'type'                      => 'switch',
                'title'                     => esc_html__( 'Full Width Footer', 'moto' ),
                'subtitle'                  => esc_html__( 'Turn on to make the footer area full width', 'moto' ),
                'default'                   => false,
            ),
            array(
                'id'                        => 'moto_footer_widget_height',
                'type'                      => 'spacing',
                'title'                     => esc_html__( 'Footer Padding', 'moto' ),
                'subtitle'                  => esc_html__( 'Controls the top and bottom padding for the footer. Enter values excluding any valid CSS unit, ex: 50, 0,', 'moto' ),
                'output'                    => array('.single-footer'),
                'mode'                      => 'margin',
                'units_extended'            => 'false',
                'left'                      => 'false',
                'right'                     => 'false',
                'units'                     => array('em','px'),
                'default'                   => array(
                    'margin-top'            => 'px',
                    'margin-right'          => 'px',
                    'margin-bottom'         => 'px',
                    'margin-left'           => 'px',
                    'units'                 => 'px',
                )
            ),
            
        )
    ));
    //footer typography
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__('Footer Typography', 'moto'),
        'id'               => 'moto_footer_typography',
        'subsection'       => true,
        'icon'             => 'el el-arrow-right',
        'fields'           => array( 
                array(
                    'id'                    => 'moto_footerfont',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Widget Title Typography', 'moto'),
                    'google'                => true,       
                    'subsets'               => false, 
                    'text-transform'        => true, 
                    'letter-spacing'        => true, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('.footer-title'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('Controls the font settings of the widget title', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_footerfontbody',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Footer Body Typography', 'moto'),
                    'google'                => true,          
                    'subsets'               => false, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('.footer-brief, .textwidget'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('Controls the font settings of the footer body', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                'id'                        => 'moto_footer_link',
                'type'                      => 'color',
                'output'                    => array('.footer-top-area a, .single-footer ul li a, .single-footer .footer-social li a'),
                'title'                     => esc_html__( 'Footer Link Color', 'moto' ),
                'subtitle'                  => esc_html__( 'Controls the footer link color.', 'moto' ),
                ),
                array(
                    'id'                    => 'moto_footer_link_hover',
                    'type'                  => 'color',
                    'output'                => array( '.single-footer a:hover, .single-footer ul li a:hover'),
                    'title'                 => esc_html__( 'Footer Link Hover Color', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the footer link hover color .', 'moto' ),
                ),
            )
        ) );

    Redux::setSection( $opt_name, array(
        'title'            => esc_html__('Copyright Style', 'moto'),
        'id'               => 'moto_footer_style',
        'subsection'       => true,
        'icon'             => 'el el-arrow-right',
        'fields'           => array( 
            array(
                    'id'                    => 'moto_footer_bottom_show',
                    'type'                  => 'switch',
                    'title'                 => esc_html__( 'Copyright Area', 'moto' ),
                    'subtitle'              => esc_html__( 'Turn on to display the copyright area.', 'moto' ),
                    'default'               => true,
            ),
             array(
                'id'                        => 'moto_copyright_style',
                'title'                     => esc_html__('Copyright Style', 'moto'),
                'subtitle'                  => esc_html__('Controls the Copyright style.', 'moto'),
                'type'                      => 'image_select',
                'options'                   => array(
                    '1'                     => array(
                        'title'             => esc_html__('Copyright Style One', 'moto'),
                        'img'               => get_template_directory_uri().'/images/optionframework/copyright1.png',
                    ),
                    '2'                     => array(
                        'title'             => esc_html__('Copyright Style Two', 'moto'),
                        'img'               => get_template_directory_uri(). '/images/optionframework/copyright2.png',
                    )
                ),
                'default'                   => '1'
                ),
                array(
                    'id'                    => 'moto_footer_copyright_width',
                    'type'                  => 'switch',
                    'required'              => array('moto_copyright_style','equals', 1),
                    'title'                 => esc_html__( 'Full width Footer', 'moto' ),
                    'subtitle'              => esc_html__( 'Turn on to make full width copyright area', 'moto' ),
                    'default'               => false,
                ),
                array(
                    'id'                    => 'moto_copyright_column',
                    'type'                  => 'image_select', 
                    'required'              => array('moto_copyright_style','equals', 1), 
                    'title'                 => esc_html__('Footer Copyright Style','moto'),
                    'subtitle'              => esc_html__( 'Choose footer copyright style', 'moto' ),
                    'options'               => array(
                        '1'                 => array(
                            'title'         => esc_html__('Style One','moto'),
                            'img'           => get_template_directory_uri().'/images/optionframework/fs-1.png',
                        ),
                        '2'                 => array(
                            'title'         =>  esc_html__('Style Two','moto'),
                            'img'           => get_template_directory_uri(). '/images/optionframework/fs-2.png',
                        ),
                        '3'                 => array(
                            'title'         => esc_html__('Style Three','moto'),
                            'img'           => get_template_directory_uri(). '/images/optionframework/fs-3.png',
                        )
                    ),  
                    'default'               => '2',
                ),
                array(
                    'id'                    => 'moto_footer',
                    'type'                  => 'sorter', 
                    'required'              => array('moto_copyright_style','equals', 1),    
                    'title'                 => esc_html__( 'Footer Copyright Content','moto'),
                    'subtitle'              => esc_html__( 'Enable and disable footer copyright area content just by dragging and dropping.', 'moto' ),
                    'options'               => array(
                        'enabled'           => array(
                        'copyright'         => esc_html__('Copyright','moto'),
                        'socialicon'        => esc_html__('Social icon','moto')
                        ),
                        'disabled'          => array(
                        'copyrightmenu'     => esc_html__('Menu','moto')
                        )
                    )               
                ),
                array(
                    'id'                    => 'moto_copyright',
                    'type'                  => 'editor',
                    'required'              => array('moto_copyright_style','equals', 1), 
                    'title'                 => esc_html__('Copyright Content', 'moto'),
                    'subtitle'              => esc_html__('Enter your copyright content here. HTML tags allowed: a, br, em, strong', 'moto'),
                    'args'                  => array(
                    'wpautop'               => false,
                    'teeny'                 => true,
                    'textarea_rows'         => 5
                    )
                ),
				// Phone
				array(
                    'id'                    => 'moto_phone_icon',
                    'type'                  => 'text',
					'required'              => array('moto_copyright_style','equals', 2), 
                    'title'                 => esc_html__('Your Phone Icon', 'moto'),
                    'default'               => esc_html__('icofont icofont-phone', 'moto'),
					'subtitle'              => esc_html__('Enter your icon class copy and paste the text box', 'moto'),
                ), 
				array(
                    'id'                    => 'moto_phone_text',
                    'type'                  => 'text',
					'required'              => array('moto_copyright_style','equals', 2), 
                    'title'                 => esc_html__('Your Frist Phone No:', 'moto'),
                    'default'               => esc_html__('+012 345 678 102', 'moto'),
					'subtitle'              => esc_html__('Enter your Text on text box', 'moto'),
                ), 
				array(
                    'id'                    => 'moto_phone_text_2',
                    'type'                  => 'text',
					'required'              => array('moto_copyright_style','equals', 2), 
                    'title'                 => esc_html__('Text Two', 'moto'),
                    'default'               => esc_html__('+012 345 678 102', 'moto'),
					'subtitle'              => esc_html__('Enter your text on text box', 'moto'),
                ), 
				// Mail
				array(
                    'id'                    => 'moto_mail_icon',
                    'type'                  => 'text',
					'required'              => array('moto_copyright_style','equals', 2), 
                    'title'                 => esc_html__('Your Mail Icon', 'moto'),
                    'default'               => esc_html__('icofont icofont-web', 'moto'),
					'subtitle'              => esc_html__('Enter your icon class copy and paste the text box', 'moto'),
                ), 
				array(
                    'id'                    => 'moto_mail_text',
                    'type'                  => 'text',
					'required'              => array('moto_copyright_style','equals', 2), 
                    'title'                 => esc_html__('Text One', 'moto'),
                    'default'               => esc_html__('urname@email.com', 'moto'),
					'subtitle'              => esc_html__('Enter your Text on text box', 'moto'),
                ), 
				array(
                    'id'                    => 'moto_mail_text_2',
                    'type'                  => 'text',
					'required'              => array('moto_copyright_style','equals', 2), 
                    'title'                 => esc_html__('Text Two', 'moto'),
                    'default'               => esc_html__('urwebsitenaem.com', 'moto'),
					'subtitle'              => esc_html__('Enter your text on text box', 'moto'),
                ), 
				// Address
				array(
                    'id'                    => 'moto_address_icon',
                    'type'                  => 'text',
					'required'              => array('moto_copyright_style','equals', 2), 
                    'title'                 => esc_html__('Your Address Icon', 'moto'),
                    'default'               => esc_html__('icofont icofont-social-google-map', 'moto'),
					'subtitle'              => esc_html__('Enter your icon class copy and paste the text box', 'moto'),
                ), 
				array(
                    'id'                    => 'moto_address_text',
                    'type'                  => 'text',
					'required'              => array('moto_copyright_style','equals', 2), 
                    'title'                 => esc_html__('Text One', 'moto'),
                    'default'               => esc_html__('ur address goes here,', 'moto'),
					'subtitle'              => esc_html__('Enter your Text on text box', 'moto'),
                ), 
				array(
                    'id'                    => 'moto_address_text_2',
                    'type'                  => 'text',
					'required'              => array('moto_copyright_style','equals', 2), 
                    'title'                 => esc_html__('Text Two', 'moto'),
                    'default'               => esc_html__('street,Crossroad123.', 'moto'),
					'subtitle'              => esc_html__('Enter your text on text box', 'moto'),
                ),
                array(
                    'id'                    => 'moto_copyright2_logo',
                    'type'                  => 'media',
                     'required'             => array('moto_copyright_style','equals', 2),
                    'title'                 => esc_html__('Copyright Logo', 'moto'),
                    'subtitle'              => esc_html__('Upload logo', 'moto'),
                    'default'               => array(
                        'url'               => get_template_directory_uri().'/images/logo.png'
                    ),
                ),
                array(
                    'id'                    => 'moto_copyright_copany_info',
                    'type'                  => 'editor',
                     'required'             => array('moto_copyright_style','equals', 2),
                    'title'                 => esc_html__('Company Information', 'moto'),
                    'subtitle'              => esc_html__('Enter your company information here. HTML tags allowed: a, br, em, strong', 'moto'),
                    'default'               => esc_html__('', 'moto'),
                    'args'                  => array(
                    'wpautop'               => false,
                    'teeny'                 => true,
                    'textarea_rows'         => 5
                    )
                ),

                array(
                    'id'                    => 'moto_copyright_social_icon_show',
                    'type'                  => 'switch',
                     'required'             => array('moto_copyright_style','equals', 2),
                    'title'                 => esc_html__('Copyright Social Icon Show Hide', 'moto'),
                    'subtitle'              => esc_html__('Controls copyright social icon show or hide', 'moto'),
                    'default'               => true,
                ),
                array(
                    'id'                    => 'moto_copyright_info_two',
                    'type'                  => 'editor',
                     'required'             => array('moto_copyright_style','equals', 2),
                    'title'                 => esc_html__('Company Content', 'moto'),
                    'subtitle'              => esc_html__('Enter your company information here. HTML tags allowed: a, br, em, strong', 'moto'),
                    'default'               => esc_html__('Copyright Devitems. All Rights Reserved', 'moto'),
                    'args'                  => array(
                    'wpautop'               => false,
                    'teeny'                 => true,
                    'textarea_rows'         => 5
                    )
                ),


             )
        ) );
    //footer copyright
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__('Copyright Typography', 'moto'),
        'id'               => 'moto_footer_id',
        'subsection'       => true,
        'icon'             => 'el el-arrow-right',
        'fields'           => array(  
                array(
                    'id'                    => 'moto_footer_copyright_height',
                    'type'                  => 'spacing',
                    'title'                 => esc_html__( 'Footer Copyright Padding', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the top and bottom padding of the copyright area. Enter values excluding any valid CSS unit, ex: 20, 20,', 'moto' ),
                    'output'                => array('.footer-bottom'),
                    'mode'                  => 'padding',
                    'units_extended'        => 'false',
                    'left'                  => 'false',
                    'right'                 => 'false',
                    'units'                 => array('em','px'),
                    'default'               => array(
                        'padding-top'       => 'px',
                        'padding-right'     => 'px',
                        'padding-bottom'    => 'px',
                        'padding-left'      => 'px',
                        'units'             => 'px',
                    ),
                ),
                array(
                    'id'                    => 'moto_copyrightbg_color',
                    'type'                  => 'background',
                    'background-attachment' => true,
                    'background-size'       => true,
                    'background-position'   => true,
                    'background-image'      => false,
                    'background-repeat'     => true,
                    'output'                => array( '.footer-bottom' ),
                    'title'                 => esc_html__( 'Footer Copyright Background Color', 'moto' ),
                    'subtitle'              => esc_html__( 'Control the background color of the copyright area(default: #222222 ).', 'moto' ),
                    'default'               => array(
                        'google'                => true,
                        'background-color'      => '#1e2127'

                    )
                ),
                array(
                    'id'                    => 'moto_footercopyright',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('Footer Copyright Typography', 'moto'),
                    'google'                => true,     
                    'subsets'               => false, 
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('.copyright-text,.copyright-text > p'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('Controls the font settings of the footer copyright text', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_footer_copyright',
                    'type'                  => 'color',
                    'output'                => array( '.copyright-text a,.footer-bottom ul li a'),
                    'title'                 => esc_html__( 'Footer Copyright Link Color', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the link color of the copyright text', 'moto' ),
                ),
                array(
                    'id'                    => 'moto_footer_copyright_hover',
                    'type'                  => 'color',
                    'output'                => array( '.copyright-text a:hover, .footer-bottom ul li a:hover'),
                    'title'                 => esc_html__( 'Footer Copyright Link Hover Color', 'moto' ),
                    'subtitle'              => esc_html__( 'Controls the link hover color of the copyright text(default: #ff7f00).', 'moto' ),
                ),
            )
        )
    );


    //404 error page
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('404 Page', 'moto'),
        'id'        => 'moto_error_page',  
        'icon'      => 'el-icon-picture',
        'fields'    => array(                
                array(
                    'id'        => 'background_404',
                    'type'      => 'background',
                    'output'    => array('.error-area'),
                    'title'     => esc_html__('404 Page Background', 'moto'),
                    'subtitle'  => esc_html__('Controls the background of 404 page.', 'moto'),
                    'default'   => array(
                    )
                ),
                array(
                    'id'                    => 'moto_404_control',
                    'type'                  => 'button_set',
                    'title'                 => esc_html__('404 Style', 'moto'),
                    'subtitle'              => esc_html__('Controls the 404 title or image.', 'moto'),
                    'options'               => array(
                        'title'             => esc_html__('Title',  'moto'),
                        'image'             => esc_html__('Image', 'moto'), 
                     ), 
                    'default'               => 'title'
                ),
                 array(
                    'id'                    => 'moto_404_title',
                    'type'                  => 'text',
                    'required'              => array('moto_404_control','equals','title'),
                    'title'                 => esc_html__('Title', 'moto'),
                    'subtitle'              => esc_html__('Insert your page not found page title ( 404 )', 'moto'),
                    'value'                 => '404',
                    'default'               => '404'
                ),
                 array(
                    'id'                    => 'moto_404_img',
                    'type'                  => 'media',
                    'required'              => array('moto_404_control','equals','image'),
                    'title'                 => esc_html__('Title or 404 Image', 'moto'),
                    'subtitle'              => esc_html__('Upload your 404 image.', 'moto'),
                ),
                array(
                    'id'                    => 'moto_404_subtitle',
                    'type'                  => 'text',
                    'title'                 => esc_html__('Sub Title', 'moto'),
                     'subtitle'             => esc_html__('Insert your page not found page subtitle.', 'moto'),
                    'default'               => esc_html__('PAGE NOT FOUND', 'moto'),
                ), 
                 array(
                    'id'                    => 'moto_404_info',
                    'type'                  => 'editor',
                    'title'                 => esc_html__('Information', 'moto'),
                    'default'               => esc_html__('The page you are looking for does not exist or has been moved.', 'moto'),
                    'args'                  => array(
                        'teeny'             => true,
                        'textarea_rows'     => 5,
                    )
                ),
                array(
                    'id'                    => 'moto_button_text',
                    'type'                  => 'text',
                    'title'                 => esc_html__('Button Text', 'moto'),
                    'default'               => esc_html__('Go back to home page', 'moto'),
                ), 
                array(
                    'id'                    => 'moto_404font',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('404 Page Title Font', 'moto'),
                    'google'                => true,    
                    'font-style'            => false,    
                    'font-weight'           => false, 
                    'font-family'           => false,
                    'subsets'               => false,
                    'line-height'           => false,
                    'text-align'            => false,
                    'all_styles'            => true,    
                    'output'                => array('.pnf-inner > h1'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('404 page font.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ), 
                array(
                    'id'                    => 'moto_404font_subtitle',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('404 Page Title Font', 'moto'),
                    'google'                => true,    
                    'font-backup'           => false,    
                    'subsets'               => false, 
                    'line-height'           => false,
                    'text-align'            => false,
                    'text-transform'        => true,    
                    'all_styles'            => true,    
                    'output'                => array('.pnf-inner > h2'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('404 page font.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
                array(
                    'id'                    => 'moto_404font_sub',
                    'type'                  => 'typography',
                    'title'                 => esc_html__('404 Page Sub Title Font', 'moto'),
                    'google'                => true,    
                    'font-backup'           => false,    
                    'subsets'               => false, 
                    'line-height'           => false,
                    'text-align'            => false,
                    'text-transform'        => true,    
                    'all_styles'            => true,    
                    'output'                => array('.pnf-inner > p'), 
                    'units'                 => 'px',
                    'subtitle'              => esc_html__('404 page sub title font.', 'moto'),
                    'default'               => array(
                        'google'            => true,
                    )
                ),
            )
    ) );
    //layout
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Extra', 'moto' ),
        'id'               => 'moto_extra',
        'customizer_width' => '400px',
        'icon'             => 'el el-arrow-right',
        'fields'           => array(
                array(
                    'id'                    => 'moto_scroll_button_color',
                    'type'                  => 'color',
                    'title'                 => esc_html__('Scroll To Top Button Color', 'moto'),
                    'subtitle'              => esc_html__('Controls scroll to top button background color', 'moto'),
                ),
                array(
                    'id'                    => 'moto_blog_pagination_color',
                    'type'                  => 'color',
                    'output'                => array(' '),
                    'title'                 => esc_html__('Pagination Hover & Active Background Color', 'moto'),
                    'subtitle'              => esc_html__('Controls the background color of pagination when it is hovered over and active', 'moto'),
                ),
            )
        ) 
    );