<?php 
	/*
	 * Moto Main menu
	 * Author: codecarnival
	 * Author URI: http://hastech.company
	 * Version: 1.0.0
	 * ======================================================
	 */

	$moto_opt = moto_get_opt();

	$header_widht = $moto_opt['moto_header_full_width'];
	if( isset( $header_widht ) && true == $header_widht ){
		$header_widht = 'container-fluid';
	}else {
		$header_widht = 'container';
	}


	$moto_header_sticky_class = "";
	$moto_header_sticky = $moto_opt['moto_header_sticky'];
	if ( isset( $moto_header_sticky ) && true == $moto_header_sticky ) {
	$moto_header_sticky_class = "header-sticky";
	}

	$moto_header_transparent_class = "";
	$moto_header_transparent = $moto_opt['moto_header_transparent'];
	if ( isset( $moto_header_transparent ) && true == $moto_header_transparent ) {
	$moto_header_transparent_class = "header-transparent";
	}


?>
<div class="header-area <?php echo esc_attr( $moto_header_sticky_class ); ?> <?php echo esc_attr( $moto_header_transparent_class ); ?>">
	<div class="<?php echo esc_attr( $header_widht ); ?>">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<?php 
					$moto_logo_position = $moto_opt['moto_logo_position'];

					if (isset( $moto_logo_position )) {
						$moto_logo_position_value = $moto_logo_position;
					}
				?>
				<div class="header-menu-wrap logo-<?php echo esc_attr( $moto_logo_position_value ); ?> ">
					<div class="site-title">
						<?php 
							if( isset( $moto_opt['moto_head_logo']['url'])){
						?>
							<a href="<?php echo esc_url( home_url('/')); ?>" title="<?php echo esc_attr( get_bloginfo('name','moto')); ?>" rel="home" >
							<?php if ( $moto_opt['moto_main_logo']=='1'){ ?>
							<img src="<?php  echo esc_url( $moto_opt['moto_head_logo']['url']); ?>" alt="<?php  echo get_bloginfo('name'); ?>">
								<?php } else{
									 if( $moto_opt['moto_main_logo']=='2' ){echo esc_html( $moto_opt['moto_logo_text'] );} 
									}?>
							</a>
						<?php
							}else{ ?> 
								<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo esc_html( $moto_opt['moto_logo_text'] ); ?></a></h1>
							<?php }
						?>
					</div>
					<div class="primary-nav-wrap primary-nav-one-page nav-horizontal uppercase nav-effect-1">
						<nav>
							<?php
								wp_nav_menu(array(
									'theme_location' => 'onepage',
									'container'      => false,
									'fallback_cb'    => 'moto_fallback'
								));
							?>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!-- Mobile Menu  -->
		<div class="mobile-menu"></div>
	</div>
</div>
			