<?php

add_theme_support( 'woocommerce' );

function moto_woocommerce_widget(){
	if(class_exists('Woocommerce')){
		register_sidebar( array(
			'name'          => esc_html__( 'Woocommerce', 'moto' ),
			'id'            => 'sidebar-shop',
			'description'   => esc_html__( 'Add widgets here.', 'moto' ),
			'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5 class="sidebar-title">',
			'after_title'   => '</h5>',
		) );
	}
}
add_action( 'widgets_init', 'moto_woocommerce_widget' );

 add_theme_support( 'wc-product-gallery-zoom' );
 add_theme_support( 'wc-product-gallery-lightbox' );
 add_theme_support( 'wc-product-gallery-slider' );
 

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

// Change products per page
function moto_woocommerce_change_per_page() {
	$wphash_opt = moto_get_opt();

	return $wphash_opt['product_per_page'];
}
add_filter( 'loop_shop_per_page', 'moto_woocommerce_change_per_page', 20 );
 
add_action('woocommerce_checkout_after_customer_details','moto_chckout_order_start',10);
add_action('woocommerce_checkout_after_order_review','moto_chckout_order_end',15);
 
 function moto_chckout_order_start(){
	 echo ' <div class="checkout-order">';
 }

 function moto_chckout_order_end(){
	 echo '</div>';
 }