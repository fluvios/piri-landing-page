<?php
	$moto_opt = moto_get_opt();

	$moto_banner_bg_color = $moto_banner_bg_repeat = $moto_banner_bg_size = $moto_banner_bg_attacement = $moto_banner_bg_position = $moto_banner_bg_image = "";	

	if ( isset( $moto_opt['moto_breadcrumbs_bg_optins'] ) ) {
		$moto_banner_bg_options = $moto_opt['moto_breadcrumbs_bg_optins'];	
	}

	if(isset($moto_banner_bg_options['background-color'])){
		$moto_banner_bg_color = $moto_banner_bg_options['background-color'];
	}

	if(isset($moto_banner_bg_options['background-repeat'])){
		$moto_banner_bg_repeat = $moto_banner_bg_options['background-repeat'];
	}

	if(isset($moto_banner_bg_options['background-size'])){
		$moto_banner_bg_size = $moto_banner_bg_options['background-size'];
	}

	if(isset($moto_banner_bg_options['background-attachment'])){
		$moto_banner_bg_attacement = $moto_banner_bg_options['background-attachment'];
	}

	if(isset($moto_banner_bg_options['background-position'])){
		$moto_banner_bg_position = $moto_banner_bg_options['background-position'];
	}
	if(isset($moto_banner_bg_options['background-image'])){
		$moto_banner_bg_image = $moto_banner_bg_options['background-image'];
	}

	$_motoid = get_the_ID();
	$moto_banner_img = get_post_meta($_motoid,'_moto_banner_img',true);
	$moto_banner_background = get_post_meta($_motoid,'_moto_banner_color',true);

?>


<?php

  //show bar and content
  if( '1' == isset($moto_opt['moto_page_title_bar']) ? $moto_opt['moto_page_title_bar'] : '' ){ ?>
	<?php
	//show content only
 }elseif( '2' == isset($moto_opt['moto_page_title_bar']) ? $moto_opt['moto_page_title_bar'] : '' ) {


 }else { ?>
	<div class="breadcrumbs-area breadcrumbs-bg breadcrumbs-area-default">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="breadcrumbs breadcrumbs-title-left">
						<h2 class="page-title"><?php wp_title(''); ?></h2>
						<div class="page-title-bar">
							<?php 

								if(function_exists('woocommerce_breadcrumb')){
									if(is_woocommerce()){
										woocommerce_breadcrumb();
									} else {
										moto_breadcrumbs();
									}
								} else {
									moto_breadcrumbs();
								}

							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
<?php }
