<?php
/**
 * This partial is used for displaying the Site Header when in archive pages
 *
 * @package moto
 * @since moto 1.0.0
 */
$moto_opt = moto_get_opt();

$moto_page_titlebar = get_post_meta( get_the_id(),'_moto_page_titlebar_enable',true);

	if ( is_home() ) : 
		if( isset($moto_opt['moto_bolg_title_bar']) ? $moto_opt['moto_bolg_title_bar'] : '' ==true || isset($moto_opt['moto_bolg_title_bar']) ? $moto_opt['moto_bolg_title_bar'] : '' ==''):
?>
<section class="breadcrumbs-area bredcrumb-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="breadcrumb-text">
					<h1><?php moto_blog_header(); ?></h1>
					<?php 
						if( !empty($moto_opt['moto_bolg_subtitle']) ):
					?>
					<h3><?php echo  esc_html( $moto_opt['moto_bolg_subtitle'] ); ?></h3>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
	endif;

	elseif(!is_front_page() && is_page() )  : 
		if( $moto_page_titlebar == 'yes' || $moto_page_titlebar == '' ):
	 	get_template_part('/inc/bredcrumb/pagetitle');
	endif;

	//archive page
 	elseif( is_archive() ) : 
		get_template_part('/inc/bredcrumb/pagetitle');
	
 	elseif(is_single()) : 
		if( $moto_page_titlebar == 'yes' || $moto_page_titlebar == '' ): 
			get_template_part('/inc/bredcrumb/pagetitle');
		endif;
	//404 page --->
 	elseif(is_404()) : 
		get_template_part('/inc/bredcrumb/pagetitle');
	//search page--->
 	elseif(is_search()) : 
		get_template_part('/inc/bredcrumb/pagetitle');
 	else : 
 endif;