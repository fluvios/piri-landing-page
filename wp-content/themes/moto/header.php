<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moto
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>
	<?php

		$moto_opt = moto_get_opt();

		$moto_layout_width = isset($moto_opt['moto_layout_width']) ? $moto_opt['moto_layout_width'] : '';
		
		if ( isset( $moto_layout_width ) ) {
			$moto_layout_width_value = $moto_layout_width;
		} else {
			$moto_layout_width_value = $moto_layout_width;
		};


	?>
<body <?php body_class(); ?>>

	<div id="page" class="site site-wrapper <?php echo esc_attr( $moto_layout_width_value ); ?>">
	<div id="moto">

		<header>
			
			<?php  get_template_part('/inc/header/header-top-bar');

				$onepage = get_post_meta(get_the_id(),'_moto_page_menu_style',true);
				
				if( $onepage == 'one_page' || isset($moto_opt ['moto_header_layout']) ? $moto_opt ['moto_header_layout'] : '' == '3' ){
					get_template_part('/inc/header/header-one-page');
				}elseif( isset($moto_opt ['moto_header_layout']) ? $moto_opt ['moto_header_layout'] : '' == '2'){
					get_template_part('/inc/header/header-2');
				}else{
					get_template_part('/inc/header/header-1');
				}
			?>
			
		</header>
	<div id="content" class="site-content">

	<?php get_template_part('/inc/bredcrumb/header-page-title'); ?>
