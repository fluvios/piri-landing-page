<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moto
 */

?>

	</div><!-- #content -->
	<!-- START FOOTER AREA -->
	<footer id="footer" class="footer-area">
		<?php moto_footer_area(); ?>
		<?php moto_copyright_text(); ?>
	</footer>
</div><!-- #page -->
</div>

<?php wp_footer(); ?>

</body>
</html>
