<?php 
/*
 * Apps Screenshot ShortCode
 * Author: Hastech
 * Author URI: http://hastech.company
 * Version: 1.0.0
 * ======================================================
 * 
/**
 * =======================================================
 *    KC Shortcode Map
 * ======================================================= */
  add_action('init', 'moto_apps_scrrenshot_section'); // Call kc_add_map function ///
if(!function_exists('moto_apps_scrrenshot_section')):
	function moto_apps_scrrenshot_section(){
		if(function_exists('kc_add_map')): // if kingComposer is active
		kc_add_map(
		    array(
		        'moto_appsscreenshot' => array( // <-- shortcode tag name
		            'name' => esc_html__('Apps Screenshot Slider', 'moto'),
		            'description' => esc_html__('Description Here', 'moto'),
		            'icon' => 'fa-header',
		            'category' => 'Moto',
		            'params' => array(
		        // .............................................
		        // ..... // Content TAB
		        // .............................................
		         	'General' => array(
							array(
								'type'			=> 'group',
								'label'			=> esc_html__(' logo', 'moto'),
								'name'			=> 'images',
								'description'	=> esc_html__( 'Repeat this fields with each item created, Each item corresponding an icon element.', 'moto' ),
								'options'		=> array('add_text' => __(' Add new icon', 'moto')),
							
								'value'     => base64_encode( json_encode(array( ) ) 
							),
							'params' => array(
								array(
									'type'       => 'attach_image',
									'label'      => esc_html__( 'Add Brand logo', 'moto' ),
									'name'        => 'image',
									'description' => esc_html__( 'Enter text used as title of the icon.', 'moto' ),
									'admin_label' => true,
								),

							),
						),
		                array(
		                    'name'        => 'custom_css_class',
		                    'label'       => esc_html__('CSS Class','moto'),
		                    'description' => esc_html__('Custom css class for css customisation','moto'),
		                    'type'        => 'text'
		                ),
		         	), // content
		        // .............................................
		        // ..... // Styling
		        // .............................................
		                    'styling' => array(
		                    	array(
		                    		'name'   => 'custom_css',
		                    		'type'   => 'css',
		                    		'options' => array(
		                    			array(
		                    				'screens' => 'any,1024,999,767,479',
		                    				'Image Style'   => array(
		                    					array( 
		                    					    'property' => 'box-shadow',
													'name'     =>  'images',
		                    					    'label'    =>  esc_html__('Image Box Shadow','moto'), 
		                    					    'selector' => '+ .single-screenshot .image' 
		                    					),
												array( 
		                    					    'property' => 'border',
													'name'     =>  'images',
		                    					    'label'    =>  esc_html__('Image Box Border','moto'), 
		                    					    'selector' => '+ .single-screenshot .image' 
		                    					)
		                    				),
											////
											'Box' => array(
												array(
												'property'  => 'margin',
												'label'     => esc_html__('Box Margin','moto'),
												'selector'  => '+ .screenshot-slider ',
												),
												array(
												'property'  => 'padding',
												'label'     => esc_html__('Box Margin','moto'),
												'selector'  => '+ .screenshot-slider ',
												),
											),
		                    			)
		                    		) //End of options
		                    	)
		                    ), //End of styling
                            'animate' => array(
								array(
									'name'    => 'animate',
									'type'    => 'animate'
								)
							), //End of animate
		        // .............................................
		        // .............................................
		        // .............................................
		        /////////////////////////////////////////////////////////
		            )// Params
		        )// end shortcode key
		    )// first array
		); // End add map
		endif;
	}
endif;
		
 /*
 * =======================================================
 *    Register Brand Shortcode   
 * =======================================================
 */

 if( !function_exists('moto_apps_screenshot_shortcodess') ){
	function moto_apps_screenshot_shortcodess($atts,$content){
		$moto_apps_screenshot = shortcode_atts(array(
	   'images'           =>'',
	   'custom_css'       =>'',
	   'custom_css_class' =>'',
		),$atts); 
		extract( $moto_apps_screenshot );
		//custom class		
		$wrap_class  = apply_filters( 'kc-el-class', $atts );
		if( !empty( $custom_class ) ):
			$wrap_class[] = $custom_class;
		endif;
		$extra_class =  implode( ' ', $wrap_class );


	ob_start();
	?>
	<div class="<?php echo esc_attr( $extra_class ); ?> <?php echo esc_attr( $custom_css_class ); ?>">
	
	<div class="screenshot-area">
			<div class="col-md-12 col-xs-12">
				<div class="screenshot-slider">
					<?php foreach( $images as $item ): 
						$img = $item->image;
						$image = wp_get_attachment_image_src( $img, 'full');
						
					?>
					<div class="single-screenshot">
						<div class="image">
							<img src="<?php echo esc_url( $image[0] ); ?>" alt="">
						</div>
					</div>
					<?php endforeach; ?>					
				</div>
			</div>
        </div>
		
    </div>

	<?php
	
		return ob_get_clean();
	}
	add_shortcode('moto_appsscreenshot' ,'moto_apps_screenshot_shortcodess');
}
 