<?php 
/*
 * Apps Screenshot ShortCode
 * Author: Hastech
 * Author URI: http://hastech.company
 * Version: 1.0.0
 * ======================================================
 * 
/**
 * =======================================================
 *    KC Shortcode Map
 * ======================================================= */
  add_action('init', 'moto_team_section'); // Call kc_add_map function ///
if(!function_exists('moto_team_section')):
	function moto_team_section(){
		if(function_exists('kc_add_map')): // if kingComposer is active
		kc_add_map(
		    array(
		        'moto_team' => array( // <-- shortcode tag name
		            'name' => esc_html__('Our Team', 'moto'),
		            'description' => esc_html__('Description Here', 'moto'),
		            'icon' => 'fa-header',
		            'category' => 'Moto',
		            'params' => array(
		        // .............................................
		        // ..... // Content TAB
		        // .............................................
		         	'General' => array(
						array(
							'type'       => 'attach_image',
							'name'        => 'team_image',
							'label'      => esc_html__( 'Add Background Image', 'mobilemend' ),
							'description' => esc_html__( 'Attach Your Background Image.', 'mobilemend' ),
						
						),
						array(
							'type' => 'text',
							'name' => 'team_member_name',
							'label' => esc_html__( 'Team Member Name', 'mobilemend' ),
							'description' => esc_html__( 'Enter Your Team Member Name.', 'mobilemend' ),
							'admin_label' => true,
						),
						array(
							'type' => 'text',
							'name' => 'team_member_title',
							'label' => esc_html__( 'Team Member Title', 'mobilemend' ),
							'description' => esc_html__( 'Enter Your Team Member Title.', 'mobilemend' ),
						),
		                array(
		                    'name'        => 'custom_css_class',
		                    'label'       => esc_html__('CSS Class','moto'),
		                    'description' => esc_html__('Custom css class for css customisation','moto'),
		                    'type'        => 'text'
		                ),
		         	), // content
		        // .............................................
		        // ..... // Styling
		        // .............................................
		                    'styling' => array(
		                    	array(
		                    		'name'   => 'custom_css',
		                    		'type'   => 'css',
		                    		'options' => array(
		                    			array(
		                    				'screens' => 'any,1024,999,767,479',
											// Team Member Name
											'Title'   => array(
		                    					array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color',  'moto'),
		                    					    'selector' => '+ .team-overlay > h5' 
		                    					),
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Font Family', 'moto'),
		                    						'selector' => '+  .team-overlay > h5'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Font Size','moto'), 
		                    						'selector' => '+ .team-overlay > h5' 
		                    					),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Font Weight', 'moto'),
		                    						'selector' => '+  .team-overlay > h5'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Text Transform', 'moto'), 
													'selector' => '+  .team-overlay > h5'
												),
												array(
		                    						'property' => 'text-align', 
		                    						'label'    => esc_html__('Aliengment', 'moto'), 
		                    						'selector' => '+ .team-overlay > h5'
		                    					),
												array(
													'property'   => 'padding',
													'label'      => esc_html__('Padding','mobilemend'),
													'selector'   => '+ .team-overlay > h5',
												),
												array(
													'property'   => 'margin',
													'label'      => esc_html__('Margin','mobilemend'),
													'selector'   => '+ .team-overlay > h5',
												)
		                    				),
											// Team Member Sub
											'Sub Titel'   => array(
		                    					array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color',  'moto'),
		                    					    'selector' => '+ .team-overlay h6' 
		                    					),
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Font Family', 'moto'),
		                    						'selector' => '+  .team-overlay h6'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Font Size','moto'), 
		                    						'selector' => '+ .team-overlay h6' 
		                    					),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Font Weight', 'moto'),
		                    						'selector' => '+  .team-overlay h6'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Text Transform', 'moto'), 
													'selector' => '+  .team-overlay h6'
												),
												array(
		                    						'property' => 'text-align', 
		                    						'label'    => esc_html__('Aliengment', 'moto'), 
		                    						'selector' => '+ .team-overlay h6'
		                    					),
												array(
													'property'   => 'padding',
													'label'      => esc_html__('Padding','mobilemend'),
													'selector'   => '+ .team-overlay h6',
												),
												array(
													'property'   => 'margin',
													'label'      => esc_html__('Margin','mobilemend'),
													'selector'   => '+ .team-overlay h6',
												)
		                    				),
											// Image Style
		                    				'Image Style'   => array(
		                    					array( 
		                    					    'property' => 'box-shadow',
													'name'     =>  'images',
		                    					    'label'    =>  esc_html__('Image Shadow','moto'), 
		                    					    'selector' => '+ .team-single img' 
		                    					),
												array( 
		                    					    'property' => 'border',
													'name'     =>  'images',
		                    					    'label'    =>  esc_html__('Image Border','moto'), 
		                    					    'selector' => '+ .team-single img' 
		                    					),
												array( 
		                    					    'property' => 'border-radius',
													'name'     =>  'images',
		                    					    'label'    =>  esc_html__('Image Radius','moto'), 
		                    					    'selector' => '+ .team-single img' 
		                    					),
		                    				),
											// Overlay Box
		                    				'Overlay Box'   => array(
		                    					array(
													'property'  => 'background',
													'label'     => esc_html__('Background Color','moto'),
													'selector'  => '+ .team-overlay',
												),
												array(
													'property'  => 'box-shadow',
													'label'     => esc_html__('Shadow','moto'),
													'selector'  => '+ .team-overlay',
												),
												array(
													'property'  => 'text-align',
													'label'     => esc_html__('Text Align','moto'),
													'selector'  => '+ .team-overlay',
												),
												array(
													'property'  => 'margin',
													'label'     => esc_html__('Margin','moto'),
													'selector'  => '+ .team-overlay',
												),
												array(
													'property'  => 'padding',
													'label'     => esc_html__('Padding','moto'),
													'selector'  => '+ .team-overlay',
												),
		                    				),
											// Image Style
		                    				'Hover'   => array(
		                    					array(
													'property'  => 'box-shadow',
													'label'     => esc_html__('Box Shadow Hover','moto'),
													'selector'  => '+ .team-single:hover ',
												),
												array( 
		                    					    'property' => 'border',
													'name'     =>  'images',
		                    					    'label'    =>  esc_html__('Image Border','moto'), 
		                    					    'selector' => '+ .team-overlay' 
		                    					),
												array( 
		                    					    'property' => 'border-radius',
													'name'     =>  'images',
		                    					    'label'    =>  esc_html__('Image Radius','moto'), 
		                    					    'selector' => '+ .team-single img' 
		                    					),
		                    				),
											////
											'Box' => array(
												array(
													'property'  => 'background',
													'label'     => esc_html__('Background','moto'),
													'selector'  => '+ .team-single',
												),
												array( 
		                    					    'property' => 'border-radius',
													'name'     =>  'images',
		                    					    'label'    =>  esc_html__('Box Radius','moto'), 
		                    					    'selector' => '+ .team-single' 
		                    					),
												array(
												'property'  => 'display',
												'label'     => esc_html__('Display','moto'),
												'selector'  => '+ .team-single',
												),
												array(
												'property'  => 'margin',
												'label'     => esc_html__('Box Margin','moto'),
												'selector'  => '+ .team-single ',
												),
												array(
												'property'  => 'padding',
												'label'     => esc_html__('Box Padding','moto'),
												'selector'  => '+ .team-single ',
												),
											),
		                    			)
		                    		) //End of options
		                    	)
		                    ), //End of styling
                            'animate' => array(
								array(
									'name'    => 'animate',
									'type'    => 'animate'
								)
							), //End of animate
		        // .............................................
		        // .............................................
		        // .............................................
		        /////////////////////////////////////////////////////////
		            )// Params
		        )// end shortcode key
		    )// first array
		); // End add map
		endif;
	}
endif;
		
 /*
 * =======================================================
 *    Register Brand Shortcode   
 * =======================================================
 */

 if( !function_exists('moto_team_shortcodess') ){
	function moto_team_shortcodess($atts,$content){
	ob_start();
			$moto_team_area = shortcode_atts(array(
		   'team_image' =>'',
		   'team_member_name' =>'',
		   'team_member_title' =>'',
		   'custom_css'       =>'',
		   'custom_css_class' =>'',
			),$atts); 
			extract( $moto_team_area );
		//custom class		
		$wrap_class  = apply_filters( 'kc-el-class', $atts );
		if( !empty( $custom_class ) ):
			$wrap_class[] = $custom_class;
		endif;
		$extra_class =  implode( ' ', $wrap_class );



	?>
	<div class="<?php echo esc_attr( $extra_class ); ?> <?php echo esc_attr( $custom_css_class ); ?>">

		<div class="team-single">
			<?php $images_url = wp_get_attachment_image_src( $team_image, 'full'); ?>
			<img src="<?php echo esc_url( $images_url[0] ); ?>" alt="" />
			<div class="team-overlay text-center">
				<h5><?php echo $team_member_name; ?></h5>
				<h6><?php echo $team_member_title; ?></h6>
			</div>
		</div>

    </div>
	
	<?php
	
		return ob_get_clean();
	}
	add_shortcode('moto_team' ,'moto_team_shortcodess');
}
 