<?php 

/*
  * WPhash Moto slider Shortcode 
  * Author: Hastech
  * Author URI: http://hastech company
  * Version: 1.0.0
  *
  *
  * =======================================================
  *    Register fector_slider shortcode
  * =======================================================
*/

add_action('init', 'wphash_moto_slider_maping_func');
if(!function_exists('wphash_moto_slider_maping_func')){
    function wphash_moto_slider_maping_func(){
        if(function_exists('kc_add_map')){
            kc_add_map(
                array(
                    'wphash_fector_slider' => array(
                        'name'        => esc_html__('Moto Slider', 'moto'),
                        'icon'        => 'fa-space-shuttle',
                        'category'    => 'Moto',
                        'params'      => array(
                            'general'   => array(
                                array(
                                    'type'          => 'group',
                                    'name'          => 'fec_slider_bg',
                                    'label'         => esc_html__( 'Slider Background Image', 'moto' ),
                                    'description'   => esc_html__( 'Set your slider background image from here', 'moto' ),
                                    'options'       => array('add_text' => __('Add new bg image', 'moto')),
                                        'params' => array(
                                            array(
                                                'type'          => 'attach_image',
                                                'name'          => 'fector_slider_image',
                                                'label'         => esc_html__( 'Slider Background Image', 'moto' ),
                                                'description'   => esc_html__( '', 'moto' ),
                                            ),
                                        )
                                    ),
                                array(
                                    'type'          => 'group',
                                    'name'          => 'fec_slider_info',
                                    'label'         => esc_html__( 'Slider Information', 'moto' ),
                                    'description'   => esc_html__( 'Set your slider information from here', 'moto' ),
                                    'options'       => array('add_text' => __('Add information', 'moto')),
                                        'params' => array(
                                            array(
                                                'type'          => 'text',
                                                'name'          => 'fector_slider_title',
                                                'label'         => esc_html__( 'Title', 'moto' ),
                                                'description'   => esc_html__( '', 'moto' ),
                                            ),
                                            array(
                                                'type'          => 'textarea',
                                                'name'          => 'fector_slider_description',
                                                'label'         => esc_html__( 'Description', 'moto' ),
                                                'description'   => esc_html__( '', 'moto' ),
                                            ),
                                            array(
                                                'type'          => 'text',
                                                'name'          => 'fector_slider_button_url',
                                                'label'         => esc_html__( 'Button Url', 'moto' ),
                                                'description'   => esc_html__( 'Set your page url here', 'moto' ),
                                            ),
                                            array(
                                                'type'          => 'text',
                                                'name'          => 'fector_slider_button_info',
                                                'label'         => esc_html__( 'Button Info', 'moto' ),
                                                'description'   => esc_html__( 'Set your button text here', 'moto' ),
                                            ),
                                            array(
                                                'type'          => 'attach_image',
                                                'name'          => 'fector_slider_caption',
                                                'label'         => esc_html__( 'Slider Caption Image', 'moto' ),
                                                'description'   => esc_html__( 'Set your slider caption image here', 'moto' ),
                                            ),
                                        )
                                    ),
                            ), 
							// general
                            'styling'   => array(
                                array(
                                    'type'          => 'css',
                                    'name'          => 'custom_css',
                                    'options'       => array(
                                        array(
                                            'screens' => 'any,1024,999,767,479',
											// Title
                                            'title'     => array(
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color',  'moto'),
		                    					    'selector' => '+ .middle-text .title-1 h1' 
		                    					),
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Font Family', 'moto'),
		                    						'selector' => '+  .middle-text .title-1 h1'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Font Size','moto'), 
		                    						'selector' => '+ .middle-text .title-1 h1' 
		                    					),
												array(
                                                    'label'     => esc_html__( 'Line Height', 'moto' ),
                                                    'property'  => 'line-height',
                                                    'selector'  => '+ .middle-text .title-1 h1',
                                                ),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Font Weight', 'moto'),
		                    						'selector' => '+  .middle-text .title-1 h1'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Text Transform', 'moto'), 
													'selector' => '+  .middle-text .title-1 h1'
												),
												array(
		                    						'property' => 'text-align', 
		                    						'label'    => esc_html__('Aliengment', 'moto'), 
		                    						'selector' => '+ .middle-text .title-1 h1'
		                    					),
												array(
													'property'   => 'padding',
													'label'      => esc_html__('Padding','moto'),
													'selector'   => '+ .middle-text .title-1 h1',
												),
												array(
													'property'   => 'margin',
													'label'      => esc_html__('Margin','moto'),
													'selector'   => '+ .middle-text .title-1 h1',
												)
                                            ),
											// Description
                                            'Description'       => array(
                                                array(
                                                    'label'     => esc_html__( 'Color', 'moto' ),
                                                    'property'  => 'color',
                                                    'selector'  => '+ .middle-text .desc p',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Font Family', 'moto' ),
                                                    'property'  => 'font-family',
                                                    'selector'  => '+ .middle-text .desc p',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Font Size', 'moto' ),
                                                    'property'  => 'font-size',
                                                    'selector'  => '+ .middle-text .desc p',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Font Weight', 'moto' ),
                                                    'property'  => 'font-weight',
                                                    'selector'  => '+ .middle-text .desc p',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Font Style', 'moto' ),
                                                    'property'  => 'font-style',
                                                    'selector'  => '+ .middle-text .desc p',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Line Height', 'moto' ),
                                                    'property'  => 'line-height',
                                                    'selector'  => '+ .middle-text .desc p',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Text Align', 'moto' ),
                                                    'property'  => 'text-align',
                                                    'selector'  => '+ .middle-text .desc p',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Text Transform', 'moto' ),
                                                    'property'  => 'text-transform',
                                                    'selector'  => '+ .middle-text .desc p',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Margin', 'moto' ),
                                                    'property'  => 'margin',
                                                    'selector'  => '+ .middle-text .desc p',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Padding', 'moto' ),
                                                    'property'  => 'padding',
                                                    'selector'  => '+ .middle-text .desc p',
                                                )
                                            ),
											// Button
                                            'Button'       => array(
                                                array(
                                                    'label'     => esc_html__( 'Color', 'moto' ),
                                                    'property'  => 'color',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
												array(
                                                    'label'     => esc_html__( 'Background Color', 'moto' ),
                                                    'property'  => 'background',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
												array(
                                                    'label'     => esc_html__( 'Font Size', 'moto' ),
                                                    'property'  => 'font-size',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Font Family', 'moto' ),
                                                    'property'  => 'font-family',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Font Weight', 'moto' ),
                                                    'property'  => 'font-weight',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Font Style', 'moto' ),
                                                    'property'  => 'font-style',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Line Height', 'moto' ),
                                                    'property'  => 'line-height',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Text Align', 'moto' ),
                                                    'property'  => 'text-align',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Text Transform', 'moto' ),
                                                    'property'  => 'text-transform',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Radius', 'moto' ),
                                                    'property'  => 'border-radius',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Display', 'moto' ),
                                                    'property'  => 'display',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Padding', 'moto' ),
                                                    'property'  => 'padding',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                ),
                                                array(
                                                    'label'     => esc_html__( 'Margin', 'moto' ),
                                                    'property'  => 'margin',
                                                    'selector'  => '+ .middle-text .contact-us a',
                                                )
                                            ),
											// Button Hover
                                            'Button Hover'       => array(
                                                array(
                                                    'label'     => esc_html__( 'Color Hover', 'moto' ),
                                                    'property'  => 'color',
                                                    'selector'  => '+ .middle-text .contact-us a:hover',
                                                ),
												array(
                                                    'label'     => esc_html__( 'Background Color Hover', 'moto' ),
                                                    'property'  => 'background',
                                                    'selector'  => '+ .middle-text .contact-us a:hover',
                                                ),
												array(
                                                    'label'     => esc_html__( 'Box Shadow Hover', 'moto' ),
                                                    'property'  => 'box-shadow',
                                                    'selector'  => '+ .middle-text .contact-us a:hover',
                                                )
                                                
                                            ),
											// Image
                                            'Image'       => array(
                                                array(
                                                    'label'     => esc_html__( 'Width', 'moto' ),
                                                    'property'  => 'width',
                                                    'selector'  => '+ .slider-caption-img img',
                                                ),
												array(
                                                    'label'     => esc_html__( 'height', 'moto' ),
                                                    'property'  => 'height',
                                                    'selector'  => '+ .slider-caption-img img',
                                                ),
												array(
                                                    'label'     => esc_html__( 'Margin', 'moto' ),
                                                    'property'  => 'margin',
                                                    'selector'  => '+ .slider-caption-img img',
                                                ),
												array(
                                                    'label'     => esc_html__( 'Padding', 'moto' ),
                                                    'property'  => 'padding',
                                                    'selector'  => '+ .slider-caption-img img',
                                                )
                                                
                                            )//social hover
                                        )
                                    )//options
                                )
                            ) //styling
                        ) //prarams
                    )
                )
            );
        }
    }
}

//[wphash_fector_slider fector_title="" fector_subtitle="" fector_content=""]
if( !function_exists ('wphash_moto_slider_shortcode_func')){
    function wphash_moto_slider_shortcode_func( $atts,$content){
        $shortcode = shortcode_atts( array(
            'fec_slider_bg' => '' , 
            'fec_slider_info' => '' , 
        ), $atts);
        extract($shortcode);
        $kc_classes = apply_filters( 'kc-el-class', $atts );
        $kc_classes = implode(' ', $kc_classes);
        ob_start();
?>
 <!--slider section start-->
	<div class="<?php echo esc_attr($kc_classes); ?>">
        <div class="slider-container slider-overlay" id="home">
            <!-- Slider Image -->
            <div id="mainSlider" class="nivoSlider slider-image">
            <?php 
            $i = 0;
                foreach( $fec_slider_bg as $key => $item ){
                    $i++;
                    $fector_single_img = $item->fector_slider_image; 
                    $get_fector_image = wp_get_attachment_image_src($fector_single_img, 'full')[0];
            ?>
                <img src="<?= $get_fector_image; ?>" alt="test" title="#htmlcaption<?= $i; ?>"/>
            <?php } ?> 
            </div>
          <?php 
          $i = 0; 
            foreach( $fec_slider_info as $keyinfo => $iteminfo ){
                $i++;
          ?>
            <!-- Slider Caption 1 -->
            <div id="htmlcaption<?= $i; ?>" class="nivo-html-caption slider-caption-1">
				<div class="container">
					<div class="row slider-cell">
						<div class="col-md-9 col-sm-12 col-xs-12" style="margin-bottom: 100px;">
							<div class="slide1-text">
								<div class="middle-text"> 
									<div class="title-1 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.3s">
										<h1><?= $iteminfo->fector_slider_title; ?></h1>
									</div>  
									<div class="desc wow fadeInLeft" data-wow-duration="1.8s" data-wow-delay="0.5s">
										<p><?= $iteminfo->fector_slider_description; ?></p>
									</div>
									<div class="contact-us wow bounceInUp" data-wow-duration="1.3s" data-wow-delay=".5s">
										<a href="<?= $iteminfo->fector_slider_button_url; ?>"><?= $iteminfo->fector_slider_button_info; ?></a>
									</div>
								</div>  
							</div>
						</div>
						<div class="col-md-3 hidden-sm hidden-xs">
							<div class="slider-caption-img wow fadeInRight" data-wow-duration="1.3s" data-wow-delay="0.5s">
							<?php 
								$fector_single_caption = $iteminfo->fector_slider_caption; 
								$get_fector_caption = wp_get_attachment_image_src($fector_single_caption, 'full')[0];
							?>
								<img src="<?= $get_fector_caption; ?>" alt="">
							</div>  
						</div>
					</div>  
				</div>
            </div>
            <?php } ?> 
        </div>
	</div>
    <!--slider section end-->   
<?php 
        return ob_get_clean();
        }
    add_shortcode('wphash_fector_slider','wphash_moto_slider_shortcode_func');
}