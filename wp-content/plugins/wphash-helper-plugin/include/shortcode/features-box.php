<?php 
/*
 * Moto Features ShortCode
 * Author: Hastech
 * Author URI: http://hastech.company
 * Version: 1.0.0
 * ======================================================
 * 
/**
 * =======================================================
 *    KC Shortcode Map
 * ======================================================= */
 add_action('init', 'moto_features_sections'); // Call kc_add_map function ///
if(!function_exists('moto_features_sections')):
	function moto_features_sections(){
		if(function_exists('kc_add_map')): // if kingComposer is active
		kc_add_map(
		    array(
		        'moto_features'  => array( // <-- shortcode tag name
		            'name'        => esc_html__('Features Box', 'moto'),
		            'description' => esc_html__('Description Here', 'moto'),
		            'icon'        => 'fa-header',
		            'category'    => 'Moto',
		            'params'      => array(
		        // .............................................
		        // ..... // Content TAB
		        // .............................................
		         	'General' => array(
						array(
							'type' => 'text',
							'name' => 'features_title',
							'label' => esc_html__( 'Features Title', 'moto' ),
							'description' => esc_html__( 'Enter Features Title.', 'moto' ),
						),
						array(
							'type' 			=> 'icon_picker',
							'name'          => 'features_icon',
							'label' 		=> esc_html__( 'Icon', 'moto' ),
							'value'			=> 'fa-play',
							'description' 	=> esc_html__( 'Select icon display on front side', 'moto' ),
						),
						array(
							'type' => 'text',
							'name' => 'features_description',
							'label' => esc_html__( 'Features Description', 'moto' ),
							'description' => esc_html__( 'Enter Your Features Description.', 'moto' ),
						),
						
		         	), // content
		        // .............................................
		        // ..... // Styling
		        // .............................................
		                    'styling' => array(
		                    	array(
		                    		'name'   => 'custom_css',
		                    		'type'   => 'css',
		                    		'options' => array(
		                    			array(
		                    				'screens' => 'any,1024,999,767,479',
		                    				'Title'   => array(
		                    					array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color',  'moto'),
		                    					    'selector' => '+ .awesome-feature-details h5' 
		                    					),
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Font Family', 'moto'),
		                    						'selector' => '+  .awesome-feature-details h5'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Font Size','moto'), 
		                    						'selector' => '+ .awesome-feature-details h5' 
		                    					),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Font Weight', 'moto'),
		                    						'selector' => '+  .awesome-feature-details h5'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Text Transform', 'moto'), 
													'selector' => '+  .awesome-feature-details h5'
												),
												array(
		                    						'property' => 'text-align', 
		                    						'label'    => esc_html__('Aliengment', 'moto'), 
		                    						'selector' => '+ .awesome-feature-details h5'
		                    					),
												array(
													'property'   => 'padding',
													'label'      => esc_html__('Padding','moto'),
													'selector'   => '+ .awesome-feature-details h5',
												),
												array(
													'property'   => 'margin',
													'label'      => esc_html__('Margin','moto'),
													'selector'   => '+ .awesome-feature-details h5',
												)
		                    				),
											/// Description Style
											'Description'   => array(
												array( 
													'property' => 'color', 
													'label'    => esc_html__( 'Description text Color', 'moto' ),
													'selector' => '+ .awesome-feature-details p' 
												),
												array( 
													'property' => 'font-size', 
													'label'    => esc_html__( 'Description Font Size', 'moto' ),
													'selector' => '+ .awesome-feature-details p' 
												),
												array(
													'property' => 'font-family', 
													'label'    => esc_html__( 'Description Font Family', 'moto' ),
													'selector' => '+ .awesome-feature-details p'
												),
												array(
													'property' => 'font-weight', 
													'label'    => esc_html__( 'Description Font Weight','moto' ), 
													'selector' => '+ .awesome-feature-details p'
												),
												array( 
													'property' => 'line-height', 
													'label'    => esc_html__( 'Description Line Height', 'moto' ),
													'selector' => '+ .awesome-feature-details p' 
												),
												array(
													'property' => 'text-transform', 
													'label'    => esc_html__( 'Description Text Transform', 'moto' ),
													'selector' => '+ .awesome-feature-details p'
												),
												array(
													'property' => 'margin', 
													'label'    => esc_html__( 'Description Text Transform', 'moto' ),
													'selector' => '+ .awesome-feature-details p'
												),
												array(
													'property' => 'padding', 
													'label'    => esc_html__( 'Description Text Transform', 'moto' ),
													'selector' => '+ .awesome-feature-details p'
												),
											),
											/// Icon
											'Icon' => array(
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color','moto'),  
		                    					    'selector' => '+ .awesome-feature-icon i' 
		                    					),
												array( 
		                    					    'property' => 'background', 
		                    					    'label'    => esc_html__('Background Color', 'moto'), 
		                    					    'selector' => '+ .awesome-feature-icon i' 
		                    					),
		                    					array( 
		                    					    'property' => 'font-size', 
		                    					    'label'    => esc_html__('Font Size','moto'),  
		                    					    'selector' => '+ .awesome-feature-icon i' 
		                    					),
		                    					array( 
		                    					    'property' => 'line-height', 
		                    					    'label'    => esc_html__('Line Hegight','moto'),  
		                    					    'selector' => '+ .awesome-feature-icon i' 
		                    					),
		                    					array( 
		                    					    'property' => 'border-radius', 
		                    					    'label'    => esc_html__('Radius','moto'),  
		                    					    'selector' => '+ .awesome-feature-icon i' 
		                    					),
		                    					array( 
		                    					    'property' => 'box-shadow', 
		                    					    'label'    => esc_html__('Shadow','moto'),  
		                    					    'selector' => '+ .awesome-feature-icon i' 
		                    					),
												array( 
		                    						'property' => 'width', 
		                    						'label'    => esc_html__('Width','moto'), 
		                    						'selector' => '+ .awesome-feature-icon i' 
		                    					),
												array(
													'property'  => 'height', 
													'label' 	=> esc_html__('Hegight', 'moto'), 
													'selector'  => '+ .awesome-feature-icon i'
												),
												array(
													'property'  => 'display', 
													'label' 	=> esc_html__('Display', 'moto'), 
													'selector'  => '+ .awesome-feature-icon i'
												),
												array(
													'property'  => 'margin', 
													'label' 	=> esc_html__('Margin', 'moto'), 
													'selector'  => '+ .awesome-feature-icon i'
												),
												array(
													'property'  => 'padding', 
													'label' 	=> esc_html__('Padding', 'moto'), 
													'selector'  => '+ .awesome-feature-icon i'
												),
											),
											/// Icon Gardaient
											'Icon Gradient' => array(
												array( 
		                    						'property' => 'width', 
		                    						'label'    => esc_html__('Width','moto'), 
		                    						'selector' => '+ .awesome-feature-icon span' 
		                    					),
												array(
													'property'  => 'height', 
													'label' 	=> esc_html__('Hegight', 'moto'), 
													'selector'  => '+ .awesome-feature-icon span'
												),
												array( 
		                    					    'property' => 'background', 
		                    					    'label'    => esc_html__('Icon Border Color', 'moto'), 
		                    					    'selector' => '+ .awesome-feature-icon span::after' 
		                    					),
		                    					array( 
		                    					    'property' => 'border-radius', 
		                    					    'label'    => esc_html__('Icon Radius','moto'),  
		                    					    'selector' => '+ .awesome-feature-icon span::after' 
		                    					),
												
											),
											///
											'Hover' => array(
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Icon Color','moto'),  
		                    					    'selector' => '+ .awesome-feature:hover .awesome-feature-icon i' 
		                    					),
												array( 
		                    					    'property' => 'font-size', 
		                    					    'label'    => esc_html__('Icon Font size', 'moto'), 
		                    					    'selector' => '+ .awesome-feature:hover .awesome-feature-icon i' 
		                    					),
												array( 
		                    					    'property' => 'background', 
		                    					    'label'    => esc_html__('Icon Background', 'moto'), 
		                    					    'selector' => '+ .awesome-feature:hover .awesome-feature-icon i' 
		                    					),
												array( 
		                    					    'property' => 'box-shadow', 
		                    					    'label'    => esc_html__('Icon Boxshadow','moto'),  
		                    					    'selector' => '+ .awesome-feature:hover .awesome-feature-icon i' 
		                    					),
											),
											///
											'Box' =>array(
		                    					array(
		                    						'property' => 'background', 
		                    						'label'    => esc_html__('Background', 'moto'),
		                    						'selector' => '+  .awesome-feature'
		                    					),
		                    					array(
		                    						'property' => 'background', 
		                    						'label'    => esc_html__('Box Hover BG', 'moto'),
		                    						'selector' => '+  .awaesome-feature:hover'
		                    					),
												array(
		                    						'property' => 'border-radius', 
		                    						'label'    => esc_html__('Button Box Radius', 'moto'),
		                    						'selector' => '+  .awesome-feature'
		                    					),
												array(
		                    						'property' => 'border', 
		                    						'label'    => esc_html__('Button Box Border', 'moto'),
		                    						'selector' => '+  .awesome-feature'
		                    					),
												array(
		                    						'property' => 'box-shadow', 
		                    						'label'    => esc_html__('Box Shadow', 'moto'),
		                    						'selector' => '+  .awesome-feature'
		                    					),
												array(
		                    						'property' => 'margin', 
		                    						'label'    => esc_html__('Button Box Shadow', 'moto'),
		                    						'selector' => '+  .awesome-feature'
		                    					),
												array(
		                    						'property' => 'padding', 
		                    						'label'    => esc_html__('Button Box Shadow', 'moto'),
		                    						'selector' => '+  .awesome-feature'
		                    					),
											),
		                    			)
		                    		) //End of options
		                    	)
		                    ), //End of styling
                            'animate' => array(
								array(
									'name'    => 'animate',
									'type'    => 'animate'
								)
							), //End of animate
		        // .............................................
		        // .............................................
		        // .............................................
		        /////////////////////////////////////////////////////////
		            )// Params
		        )// end shortcode key
		    )// first array
		); // End add map
		endif;
	}
endif;

 
 
 /*
 * =======================================================
 *    Register Brand Shortcode   
 * =======================================================
 */

 if(!function_exists('moto_features_shortcode')){
	function moto_features_shortcode( $atts, $content){
	ob_start();
			$moto_features_options = shortcode_atts(array(
			//Field Id Defind 
		   'features_icon'=>'',
		   'features_title' =>'',
		   'features_description' =>'',
		   'custom_css'       =>'',
		   'custom_css_class' =>'',
			),$atts); 
			extract( $moto_features_options );
		//custom class		
		$wrap_class  = apply_filters( 'kc-el-class', $atts );
		$wrap_class[] = 'features-area';
		if( !empty( $custom_class ) ):
			$wrap_class[] = $custom_class;
		endif;
		$extra_class =  implode( ' ', $wrap_class );
		
	?>
	<div class="<?php echo esc_attr( $extra_class ); ?> <?php echo esc_attr( $custom_css_class ); ?>">
	
		<div class="awesome-feature text-center">
			<div class="awesome-feature-icon">
				<span><i class="<?php echo $features_icon; ?>"></i></span>
			</div>
			<div class="awesome-feature-details">
				<h5><?php echo $features_title; ?></h5>
				<p><?php echo $features_description; ?></p>
			</div>
		</div>
		
	</div>
	<?php
	
		return ob_get_clean();
	}
	add_shortcode('moto_features' ,'moto_features_shortcode');
}
 