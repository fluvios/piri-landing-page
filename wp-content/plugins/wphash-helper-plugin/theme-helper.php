<?php
/**
 * Plugin Name: WPHash Helper Plugin
 * Plugin URI: https://devitems.com/
 * Description: After install the WpHash WordPress Theme, you must need to install this "WpHash Helper Plugin" first to get all functions of WPHash WP Theme.
 * Version: 1.0.0
 * Author: HasTech
 * Author URI: http://hastech.company/
 * Text Domain: wphash
 * License: GPL/GNU.
 /*Copyright 2017 WpHash(email:support@bootexperts.com)
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


//define
define( 'PLG_URL', plugins_url() );
define( 'PLG_DIR', dirname( __FILE__ ) ); 

/**----------------------------------------------------------------*/
/* Include all file
/*-----------------------------------------------------------------*/  
include_once(PLG_DIR. '/include/wphash-custom-texanomy.php');
include_once(PLG_DIR. '/shortcode-act.php');

/*--------- Include Font ----------*/
add_action('init', 'fonts_test');
function fonts_test() {
	if( function_exists( 'kc_add_icon' ) ) {   
		kc_add_icon(PLG_URL.'/wphash-helper-plugin/css/icofont.css');
	}
}